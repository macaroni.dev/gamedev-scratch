module Main where

import Demo
import CuteC2

main :: IO ()
main = demoDisplay $ drawCollided
  [ c2Circle (C2V 2.2 2.2) 100.0
  , c2Circle (C2V 50 50) 100.0
  , c2Circle (C2V 500 50) 100.0
  ]
