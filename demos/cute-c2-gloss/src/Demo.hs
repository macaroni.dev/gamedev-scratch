{-# LANGUAGE RecordWildCards #-}

module Demo where

import CuteC2
import qualified Graphics.Gloss as Gloss

import qualified Data.Map as Map

demoDisplay :: Gloss.Picture -> IO ()
demoDisplay = Gloss.display Gloss.FullScreen Gloss.white

drawCollided :: [C2Shape] -> Gloss.Picture
drawCollided shapes = foldMap drawCollision (findCollisions shapes)
  where
    drawCollision (collided, s) = case s of
      C2Circle' C2Circle{..} ->
        Gloss.Color (if collided then Gloss.red else Gloss.black) $
        Gloss.Translate (c2v_x c2Circle_p) (c2v_y c2Circle_p) $
        Gloss.ThickCircle c2Circle_r 5.0
      _ -> error "not implemented (yet)"

findCollisions :: [C2Shape] -> [(Bool, C2Shape)]
findCollisions shapes =
  let indexed = Map.fromList $ zip [0 :: Int,1..] shapes
      didCollide i shape = (any (c2Collided shape) (Map.delete i indexed), shape)
   in Map.elems $ Map.mapWithKey didCollide indexed
