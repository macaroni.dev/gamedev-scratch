self: super:

let
  hlib = self.haskell.lib;
  haskell-overlay = hself: hsuper:
    let
      ignorance = ["dist" "dist-newstyle" ".ghc.environment.*" "cabal.project.local"];
      buildDemo = name:
        hself.callCabal2nix (name +"-demo") (builtins.toPath (../../demos + "/${name}")) {};
      buildLudumDare = number: theme:
      hself.callCabal2nix ("ludum-dare${toString number}-${theme}") (builtins.toPath (../../ludum-dare + "/${toString number}-${theme}")) {};
      buildCabalSrc = name: src:
        hself.callCabal2nix name src {};
      buildCabalSubdir = name: src:
        buildCabalSrc name "${src}/${name}";
      buildCabalGit = name: git: buildCabalSrc name (self.fetchgit git);
      buildCabalGitSubdir = name: git: buildCabalSubdir name (self.fetchgit git);

      srcs = {
        generic-lens = self.fetchgit {
          url = "git://github.com/kcsongor/generic-lens.git";
          rev = "c08a67033777f0fef08aab8820a5a753a30ed961";
          sha256 = "17fz48w18g0va87maxxspwq7q18phdsmr9wimp5yr6xcqxk4w48r";
        };
      };

    in {
      animate = buildCabalGit "animate" {
        url = "https://github.com/jxv/animate.git";
        rev = "61a5b7065903b71d9accb64a0f8a3395ca54b183";
        sha256 = "0g0qfnrhja5sfa742c1nh7x13qb2jypc7sqjyrck60ca05x4n2lf";
      };
      indexed-profunctors = buildCabalGitSubdir "indexed-profunctors" {
          url = "git://github.com/well-typed/optics.git";
          rev = "63faefed6dfc9c0908dc37f38fb8829532844300";
          sha256 = "0dmam32h41nj4pn0r2fd01lhlf6icy1fm27c810mcgxil7zb05lz";
      };
      generic-lens-core = buildCabalSubdir "generic-lens-core" srcs.generic-lens;
      generic-lens = buildCabalSubdir "generic-lens" srcs.generic-lens;
      # Dunno if either of these Tiled engines are worth it..or work at all?
      aeson-tiled = buildCabalGit "aeson-tiled" {
        url = "https://github.com/schell/aeson-tiled";
        rev = "617821e3c33a25b2de74a5e63f7f88f71ea40abd";
        sha256 = "0x4hgcl395fz3lg85kw159mqxyhzqs825nx1ijngv59hqb2mpnms";
      };
      htiled = hlib.doJailbreak (buildCabalGit "htiled" {
        url = "https://github.com/chrra/htiled";
        rev = "bc93b7c508a571edf73f952559ff2b903df4bc56";
        sha256 = "1wvwl9jjz2k6cxdi40hrs8gdik0a003bcaal408ksrwmd0jwwl1y";
      });
      cute-c2 = buildCabalGit "cute-c2" {
        url = "https://gitlab.com/macaroni.dev/cute-c2-hs.git";
        rev = "5c0f3791f626e4214a3a49e39af6935ccefbc319";
        sha256 = "1l4zgmxj8n6b33p5xjp0z0fj4wgg04fnsnhy7885qc8qqnb4z6kp";
      };
      cute-c2-gloss-demo = buildDemo "cute-c2-gloss";
      ludum-dare46-keep-it-alive = buildLudumDare 46 "keep-it-alive";
    };

in {
  haskell = super.haskell // {
    packageOverrides = super.lib.composeExtensions
      (super.haskell.packageOverrides or (self: super: {}))
      haskell-overlay;
  };
}
