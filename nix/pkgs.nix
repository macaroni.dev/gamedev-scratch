let
  rev = "37d03dd470c60e2f1e285475538cb70868dd861d";
  fork = "NixOS";
  sha256 = "1d1ida5201a9v6fjhs4skj2ixcfv50nwgh9gyyhqa7a8k3kpri0d";
  haskell-overlay = import ./overlays/haskell.nix;
  pkgs = import
  (builtins.fetchTarball { url = "https://github.com/${fork}/nixpkgs/archive/${rev}.tar.gz"; sha256 = sha256; })
  { overlays = [ haskell-overlay ]; };

in pkgs
