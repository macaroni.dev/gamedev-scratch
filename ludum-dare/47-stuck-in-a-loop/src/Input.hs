{-# LANGUAGE StrictData #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TemplateHaskell #-}
module Input where

import Prelude
import Optics
import Graphics.Gloss as Gloss
import qualified Graphics.Gloss.Interface.IO.Game as Gloss
import Linear (V2(..))
import qualified Data.Map.Strict as Map

-- TODO: Have a buffer per-key? Would make tracking things like SHing easier
--
-- Or just track how many consecutive frames it is pressed?
-- In this game, it would make pokemon-style turning w/out moving easier, and
-- we just need a count of holding to start walking
data InputState = InputState
  { mouseLoc   :: V2 Float
  , keys       :: Map.Map Gloss.Key Gloss.KeyState
  } deriving (Eq, Show)
Optics.makeFieldLabelsWith Optics.noPrefixFieldLabels ''InputState

initialInputState :: InputState
initialInputState =
  InputState
  { mouseLoc = V2 0 0
  , keys = mempty
  }

-- TODO: Modifiers
-- TODO: EventKey location
stepInputState :: Gloss.Event -> InputState -> InputState
stepInputState e s = case e of
  Gloss.EventResize _ -> s
  Gloss.EventMotion (x, y) -> s & #mouseLoc .~ V2 x y
  Gloss.EventKey key@(Gloss.Char _) keyState _ _ -> s & #keys %~ Map.insert key keyState
  _ -> s
--
