{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RecordWildCards #-}

module Render where

import qualified Graphics.Gloss as Gloss
import qualified Graphics.Gloss.Juicy as Gloss

import Control.Exception (bracket)
import Linear.V4
import Linear.V2
import qualified Data.Map.Strict as Map
import qualified Data.Text as Text
import Control.Monad.Identity(Identity(..))
import Control.Monad.Reader (ReaderT(..), runReaderT, ask)
import Data.Monoid (Ap(..))

data Priority layer = Priority
  { layer :: layer
  , rank :: Float
  } deriving (Eq, Ord, Show)

--sceneAt :: Float -> Gloss.Picture -> Scene
--sceneAt y pic = Scene $ Map.singleton (Priority Nothing y) pic

mkScene' :: Ord layer => Priority layer -> Gloss.Picture -> Scene layer
mkScene' prio pic = Scene $ Map.singleton prio pic

mkScene :: Ord layer => layer -> Float -> Gloss.Picture -> Scene layer
mkScene l f p = mkScene' (Priority { layer = l, rank =  f }) p

renderScene :: Ord layer => Scene layer -> Gloss.Picture
renderScene = mconcat . fmap snd . Map.toAscList . pictures

-- TODO : Maybe fixTranslate could be moved here.
-- We probably want to describe Scenes in our logical coordinates,
-- and then let this module handle transforming them to absolute Screen
-- coordinates


data Scene layer = Scene { pictures :: Map.Map (Priority layer) Gloss.Picture }
  deriving (Eq, Show)

instance Ord layer => Semigroup (Scene layer) where
  s1 <> s2 = Scene $ Map.unionWith (mappend @Gloss.Picture) (pictures s1) (pictures s2)

instance Ord layer => Monoid (Scene layer) where
  mempty = Scene Map.empty

data Context = Context
  { width  :: Int
  , height :: Int
  -- TODO: Offsets for relative positioning? Maybe that's not needed
  -- TODO: `local` equivalent
  -- TODO: nativeWidth & nativeHeight? For scaling? Eh
  -- TODO: assets type variable? or general reader type 
  } deriving (Show)

newtype RenderT m a = RenderT { unRenderT :: ReaderT Context m a }
  deriving newtype (Functor, Applicative, Monad)
  deriving (Semigroup, Monoid) via (Ap (RenderT m) a)

type RenderM = RenderT Identity

runRenderT :: Context -> RenderT m a -> m a
runRenderT ctx = flip runReaderT ctx . unRenderT

runRenderM :: Context -> RenderM a -> a
runRenderM ctx = runIdentity . runRenderT ctx

renderAt :: Real a => V2 a -> Gloss.Picture -> RenderM Gloss.Picture
renderAt (V2 xa ya) p = RenderT $ do
  Context{..} <- ask
  let (x, y) = (realToFrac xa, realToFrac ya)
  let (widthF, heightF) = (fromIntegral width, fromIntegral height)
  pure $ Gloss.translate (x + ((-widthF) / 2)) ((heightF / 2) - y) p

sceneAt :: Ord layer => Real a => layer -> V2 a -> Gloss.Picture -> RenderM (Scene layer)
sceneAt layer v2@(V2 _ ya) p = mkScene layer (realToFrac ya) <$> renderAt v2 p
-- @ sign

rectangleSolid :: Float -> Float -> Gloss.Picture
rectangleSolid w h = Gloss.translate (w/2) (-h/2) $ Gloss.rectangleSolid w h

squareSolid :: Float -> Gloss.Picture
squareSolid s = rectangleSolid s s

rectangleWire :: Float -> Float -> Gloss.Picture
rectangleWire w h = Gloss.translate (w/2) (-h/2) $ Gloss.rectangleWire w h

squareWire :: Float -> Gloss.Picture
squareWire s = rectangleWire s s

circleSolid :: Float -> Gloss.Picture
circleSolid r = Gloss.translate r (-r) $ Gloss.circleSolid r

circle :: Float -> Gloss.Picture
circle r = Gloss.translate r (-r) $ Gloss.circle r

thickCircle :: Float -> Float -> Gloss.Picture
thickCircle t r = Gloss.translate r (-r) $ Gloss.thickCircle t r

loadPng :: FilePath -> IO Gloss.Picture
loadPng path = do
  Gloss.Bitmap bitmap <- maybe (error $ "Unable to find file: " ++ path) id <$> Gloss.loadJuicy path
  let (w, h) = Gloss.bitmapSize bitmap
  pure $ Gloss.translate ((fromIntegral w)/2) (-(fromIntegral h)/2) $ Gloss.Bitmap bitmap
