{-# LANGUAGE TemplateHaskell #-}

module Frame where

import LD47

import Optics (At(..), Ixed, IxValue, Index)

newtype Frame = Frame { unFrame :: Int }
  deriving stock   (Show, Read)
  deriving newtype (Enum, Bounded, Eq, Ord, Num, Real, Integral)

newtype FrameMap a = FrameMap { frames :: (IntMap a) }
  deriving stock (Functor, Foldable, Traversable)
  deriving newtype (Eq, Show, Ord, Read, Semigroup, Monoid)

mkOpticsLabels ''FrameMap

instance At (FrameMap a) where
  at (Frame k) = #frames % at @(IntMap a) k

type instance Index (FrameMap a) = Frame
type instance IxValue (FrameMap a) = a
instance Ixed (FrameMap a)

-- TODO (performance): Maybe FrameMap should have a special "compression" function? And then At could use that? Basically, repeated frames of the same a should result in a single entry at the first frame (with the rest being implied)
