{-# LANGUAGE PartialTypeSignatures #-}

module LD47
  ( module X
  , module LD47
  ) where

import Control.Arrow as X (first, second)
import Data.Function as X (fix)
import Data.String as X (IsString(..))
import Control.Monad.Extra as X (whenM, unlessM, ifM, orM, andM)
import Control.Applicative as X (Alternative(..))
import Control.Monad as X (when, unless, join, guard)
import Control.Monad.Trans.Maybe as X (MaybeT(..))
import Data.IntMap as X (IntMap)
import Data.Map.Strict as X (Map)
import Data.Set as X (Set)
import Data.Sequence as X (Seq)
import Data.Vector as X (Vector, (!?), ifoldl')
import Data.Text as X  (Text)
import Data.Foldable as X (Foldable(..), traverse_, sequence_, asum, find, for_)
import Optics as X
       ( view, set, over
       , (%), (^.), (^..), (^?), (&), (.~), (%~), (<&>)
       , Zoom(..), use, preuse, assign
       , at', _head, ix
       , each, traversed, mapping, filtered, folded
       , anyOf, foldOf, foldMapOf, toListOf, headOf
       , itraverse_
       , _Just, _Nothing
       , Lens'
       , makePrisms
       )
import Optics.State.Operators as X
import Safe as X hiding (at)

import Grid as X  
import Engine as X (engine, MonadEngine(..), MonadState(..), modify)
import Linear as X (V2(..))

import Optics
import qualified Language.Haskell.TH as TH
import qualified Data.Set

mkOpticsLabels :: TH.Name -> TH.DecsQ
mkOpticsLabels = Optics.makeFieldLabelsWith Optics.noPrefixFieldLabels

-- | foldOf operators
(^+) :: (Optics.Is k Optics.A_Fold, Monoid a) => s -> Optics.Optic' k is s a -> a
(^+) = flip Optics.foldOf

infixl 8 ^+

zoomMaybeT :: (Zoom m n s t)
           => Is k An_AffineTraversal
           => Optic' k is t s
           -> m c
           -> MaybeT n c
zoomMaybeT o mc = MaybeT (zoomMaybe o mc)

foldFor :: Foldable t => Monoid c => t a -> (a -> c) -> c
foldFor = flip foldMap

setOf :: Ord a => [a] -> Set a
setOf = Data.Set.fromList

setSing :: a -> Set a
setSing = Data.Set.singleton

setMap :: Ord b => (a -> b) -> Set a -> Set b
setMap = Data.Set.map

orFalse :: Maybe Bool -> Bool
orFalse = maybe False id

ffor :: Functor f => f a -> (a -> b) -> f b
ffor = flip fmap

v2xs :: Enum a => Num a => V2 a -> a -> [V2 a]
v2xs start len = ffor [0..len-1] $ \off -> start + V2 off 0

v2ys :: Enum a => Num a => V2 a -> a -> [V2 a]
v2ys start len = ffor [0..len-1] $ \off -> start + V2 0 off

-- | Like takeWhile, but inclusive of the matching element
-- https://stackoverflow.com/a/22472610
takeUntil :: (a -> Bool) -> [a] -> [a]
takeUntil _ [] = []
takeUntil p (x:xs) = x : if p x then takeUntil p xs else []

guardM :: Monad m => Alternative m => m Bool -> m ()
guardM = (>>= guard)

(<$$>) :: (Functor f0, Functor f1) =>
          (a -> b)
       -> f1 (f0 a)
       -> f1 (f0 b)
(<$$>) = fmap . fmap

infixr 8 <$$>

-- TODO: Opposite of guard/guardM
