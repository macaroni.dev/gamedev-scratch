{-# LANGUAGE TemplateHaskell #-}
module Model (module Model, Frame(..))where

import LD47
import Grid
import Frame


data Portrait =
    PortraitGrin
  | PortraitNormal
  | PortraitMad
  | PortraitShock
  deriving (Eq, Show, Read)


type Coords = V2 Coord

data HitType = SPIKE | ARROW | HOLE
  deriving (Eq, Ord, Show, Read)
makePrisms ''HitType

data Hitbox = Hitbox
  { hitType :: HitType
  , loc :: V2 Coord
  }
  deriving (Eq, Ord, Show, Read)
mkOpticsLabels ''Hitbox


newtype PuzzleKey = PuzzleKey Int
  deriving newtype (Show, Read, Eq, Ord, Enum, Bounded, Num)

-- Future: Hole?
data Terrain =
    Floor
  | Wall
  deriving (Eq, Show, Read)

data Direction =
    UP
  | DOWN
  | LEFT
  | RIGHT
  deriving (Eq, Show, Read)

data Action =
    Walk Direction
  | Push Direction
  deriving (Eq, Show, Read)

-- TODO: Trap/door state is conflated with their defs..
-- TODO: Individual switch presses
-- OBJECTS
data SpikeState = ON | OFF deriving (Eq, Show, Read)
switchSpikeState :: Bool -> SpikeState -> SpikeState
switchSpikeState shouldSwitch spikeState =
  if shouldSwitch then
    case spikeState of
      ON -> OFF
      OFF -> ON
  else spikeState

data Spikes = Spikes
  { spikeLocs :: Set Coords
  , switchLocs :: Set Coords
  , currentState :: SpikeState
  , defaultState :: SpikeState
  }
  deriving (Eq, Show, Read)
mkOpticsLabels ''Spikes

spikesOn :: Spikes -> Bool
spikesOn Spikes{..} = currentState == ON

-- ARROWS ALWAYS GO LEFT TO RIGHT
data Arrow = Arrow
  { loc :: Coords
  , len :: Int
  }
  deriving (Eq, Show, Read)
mkOpticsLabels ''Arrow

data Orientation = HORIZONTAL | VERTICAL
  deriving (Eq, Show, Read)
makePrisms ''Orientation

data TripWire = TripWire
  { start :: Coords
  , len :: Int
  }
  deriving (Eq, Show, Read)
mkOpticsLabels ''TripWire

data ArrowState =
    Untripped
  | Tripped [Coords] Int
  deriving (Eq, Show, Read)
makePrisms ''ArrowState

isTripped :: ArrowState -> Bool
isTripped = \case
  Untripped -> False
  Tripped{} -> True

isn'tTripped :: ArrowState -> Bool
isn'tTripped = not . isTripped

data Arrows = Arrows
  { arrows :: [Arrow]
  , tripWires :: [TripWire]
  , arrowState :: ArrowState
  }
  deriving (Eq, Show, Read)
mkOpticsLabels ''Arrows

data BlockState =
    BlockWait
  | BlockFall Int
  | BlockPush Direction Int
  deriving (Eq, Show, Read)
mkOpticsLabels ''BlockState

data Block = Block
  { location :: Coords
  , blockState :: BlockState
  , initialLocation :: Coords
  }
  deriving (Eq, Show, Read)
mkOpticsLabels ''Block

data HoleState =
    Unbroken
  | Breaking Int
  | Broken
  deriving (Eq, Show, Read)
mkOpticsLabels ''HoleState

data Hole = Hole
  { location :: Coords
  , holeState :: HoleState
  }
  deriving (Eq, Show, Read)
mkOpticsLabels ''Hole

holeBreakTime :: Int
holeBreakTime = 45

-- Traps can modify the base terrain (with new walls & holes)
data Trap =
    TSpikes Spikes
  | TArrows Arrows
  | TBlock Block
  | THole Hole
  deriving (Eq, Show, Read)
makePrisms ''Trap

data Door = Door
  { location :: Coords
  , switch   :: Coords
  , open     :: Bool
  }
  deriving (Eq, Show, Read)
mkOpticsLabels ''Door

data Room = Room
  { terrain :: Grid Terrain
  , start   :: Coords
  , door    :: Door
  , traps :: [Trap]
  , ghostLimit :: Int
  , timeLimit   :: Frame
  , portrait :: Portrait
  }
  deriving (Eq, Show, Read)
mkOpticsLabels ''Room

data ActionState =
    Waiting
  | Walking { dir :: Direction, blocked :: Bool } 
  | Dead HitType -- TODO: Cause of death
  deriving (Eq, Show, Read)

mkOpticsLabels ''ActionState
makePrisms ''ActionState

data ActionSeq =
  ActionSeq
  { actionState :: ActionState
  , clock :: Frame
  }
  deriving (Eq, Show, Read)
mkOpticsLabels ''ActionSeq

data Player = Player
  { location :: V2 Coord
  , actionSeq :: ActionSeq
  } deriving (Eq, Show, Read)
mkOpticsLabels ''Player

data Ghost = Ghost
  { player :: Player
  , actions :: FrameMap Action
  }
  deriving (Eq, Show, Read)
mkOpticsLabels ''Ghost

data Ruins = Ruins
  { player      :: Player
  , ghosts      :: [Ghost]
  , actions     :: FrameMap Action
  , clock       :: Frame
  , puzzleKey   :: PuzzleKey
  , puzzle      :: ~Ruins -- This always refers to ourself on creation, so we need laziness
  , room        :: Room
  , samples     :: [Ruins]
  } deriving (Eq, Show, Read)
mkOpticsLabels ''Ruins

data Sample a = Sample
  { duration :: Frame
  , value :: a
  } deriving (Eq, Show, Read)
mkOpticsLabels ''Sample

data RewindState = RewindState
  { samples :: [Ruins]
  , destination :: Ruins
  }
  deriving (Eq, Show, Read)
mkOpticsLabels ''RewindState

data World =
    WRuins Ruins
  | WRewind RewindState
  | WTitleMenu
  | WTheEnd
  deriving (Eq, Show, Read)
makePrisms ''World

isDead :: ActionState -> Bool
isDead = \case
  Dead{} -> True
  _ -> False

dirToCoords :: Direction -> Coords
dirToCoords = \case
  UP -> V2 0 (-1)
  DOWN -> V2 0 1
  LEFT -> V2 (-1) 0
  RIGHT -> V2 1 0

playerPushing :: Coords -> Player -> Maybe Direction
playerPushing coords player =
  case player ^. #actionSeq % #actionState of
    Walking{..} -> if coords == (player ^. #location) + dirToCoords dir then Just dir else Nothing
    _ -> Nothing

actionToPush :: Action -> Action
actionToPush = \case
  Walk dir -> Push dir
  Push dir -> Push dir

isBlockFall :: BlockState -> Bool
isBlockFall = \case
  BlockFall _ -> True
  _ -> False

isBlockFalling :: Block -> Bool
isBlockFalling Block{..} = isBlockFall blockState

secondsLeft :: Ruins -> Int
secondsLeft w =
  let timeLimit = w ^. #room % #timeLimit
      timeElapsed = w ^. #clock
      framesLeft :: Frame = timeLimit - timeElapsed
      framesLeftF :: Float = fromIntegral framesLeft
   in ceiling @Float @Int $ framesLeftF / 60
