let
  ref = "20.03";
  fork = "NixOS";
  sha256 = "0182ys095dfx02vl2a20j1hz92dx3mfgz2a6fhn31bqlp1wa8hlq";
  haskell-overlay = import ./overlays/haskell.nix;
  pkgs = import
  (builtins.fetchTarball { url = "https://github.com/${fork}/nixpkgs/archive/${ref}.tar.gz"; sha256 = sha256; })
  { overlays = [ haskell-overlay ]; };

in pkgs
