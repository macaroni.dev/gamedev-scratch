# Echoes Of Ouroboros

Build for Ludum Dare 47 - the theme was "Stuck in a Loop"

How to build & run on Linux:

* You can just `nix-build` and then `./result/EchoesOfOuroboros` should Just Work. The derivation packages up the assets in the right location.
* `USE_PWD_ASSETS=1 cabal v2-run` should also Just Work if you'd rather not use Nix. The dependencies are less pinned so it's less guaranteed. But it's how I build/run it on Mac.
