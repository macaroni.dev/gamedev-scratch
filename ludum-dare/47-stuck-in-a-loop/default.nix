with import ./nix/pkgs.nix;
let game = pkgs.haskellPackages.ld47-stuck-in-a-loop;
in stdenv.mkDerivation {
  name = "EchoesOfOuroboros";
  version = "LD47_1.0";

  phases = [ "installPhase" ];

  installPhase = ''
    mkdir $out
    cp ${game}/bin/ld47-stuck-in-a-loop $out/EchoesOfOuroboros
    cp -r ${game.src}/assets $out/assets
  '';
}

