module Main where

import qualified LD48.Prep.Puhoy as LD48

main :: IO ()
main = putStrLn "Puhoy.." >> LD48.main
