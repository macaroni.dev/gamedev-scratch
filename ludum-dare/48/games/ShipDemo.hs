module Main where

import qualified LD48.Prep.ShipDemo as LD48

main :: IO ()
main = putStrLn "ShipDemo" >> LD48.main
