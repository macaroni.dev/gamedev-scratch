#! /usr/bin/env bash

set -e

mods=("LD48/Tests/Flashlight/World" "SDL/Optics" "LD48/Jam/Model" "LD48/Jam/Assets" "LD48/Assets")

# Build the library to ensure our splices are up-to-date
cabal clean
cabal build ld48

for p in ${mods[@]}; do
    raw_splices=$(cat dist-newstyle/build/x86_64-linux/ghc-8.10.4/ld48-0.1.0.0/build/src/$p.dump-splices)

    # Remove the annotation between "Splicing" and the arrow
    # NOTE: You gotta quote $raw_splices here due to newlines
    splices=$(echo "$raw_splices" | sed -e '/Splicing/,/====>/d' | cut -c 5-)

    # Remove the TemplateHaskell flag so we don't run it
    sed -i '/LANGUAGE TemplateHaskell/d' src/$p.hs

    # TH_CODE indicate that the rest of the file is TH and meant to be
    # replaced with splices
    sed -i '/TH_CODE/q' src/$p.hs

    # Again, gotta quote $splices here
    echo "$splices" >> src/$p.hs
done
