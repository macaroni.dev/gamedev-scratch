You can run from source in a `nix-shell` with `USE_PWD_ASSETS=1 cabal run jam`. If you get errors on startup about not being able to find files, it's because you forgot to enable `USE_PWD_ASSETS` and the game is looking for assets in the exe directory (which is deep in `dist-newstyle`.)

This game uses `haskell.nix` so you'll probably want to [use IOHK's binary cache](https://input-output-hk.github.io/haskell.nix/tutorials/getting-started/#setting-up-the-binary-cache) to avoid hours of compilation. There still will be some local compilation for some of our C dependencies though.
