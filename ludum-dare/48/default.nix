let
  pkgs = import ./nix/pkgs.nix;
in pkgs.haskell-nix.project {
  # 'cleanGit' cleans a source directory based on the files known by git
  src = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "ld48";
    src = ./../..;
    subDir = "ludum-dare/48";
  };
  compiler-nix-name = "ghc8104";
}
