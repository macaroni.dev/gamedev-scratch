let pkgs = import ./nix/pkgs.nix;
in (import ./default.nix).shellFor {
  #packages = ps: with ps; [ ld48 ];
  tools = {
    cabal = "latest";
    hsc2hs = "latest";
  };
  exactDeps = true; # for some reason, exactDeps don't work..but it does nix-build fine
  withHoogle = false;

  buildInputs = [ pkgs.imagemagick ];
}
