(import ./default.nix).shellFor {
  packages = ps: [ps.ld48];
  withHoogle = true;
}
