module LD48.Input where

import Control.Monad (guard)
import Data.Maybe
import Optics

import qualified SDL.Utils as SDL

-- TODO: Fold would provide composable and performant event processing
-- These combinators require multiple traversals of events per frame
-- Probably doesn't matter..but it's low-hanging perf fruit and is probably
-- as nice or better of a programming API anyways
keypresses :: [SDL.Event] -> [SDL.Keycode]
keypresses = mapMaybe $ \e -> do
  SDL.KeyboardEventData{..} <- e ^? SDL._EventPayload % SDL._KeyboardEvent
  guard (keyboardEventKeyMotion == SDL.Pressed)
  guard (not keyboardEventRepeat)
  pure $ keyboardEventKeysym ^. SDL._KeysymKeycode

wasMousePressed :: SDL.MouseButton -> [SDL.Event] -> Bool
wasMousePressed button es = not $ null $ flip mapMaybe es $ \e -> do
  SDL.MouseButtonEventData{..} <- e ^? SDL._EventPayload % SDL._MouseButtonEvent
  guard (button == mouseButtonEventButton)
  guard (mouseButtonEventMotion == SDL.Pressed)
  pure ()
