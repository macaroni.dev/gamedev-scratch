{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
--{-# LANGUAGE TemplateHaskell #-}

{-# OPTIONS_GHC -Wno-all #-}
module LD48.Prep.Puhoy where

import CuteC2
import qualified Cute.Sound.C as CS
import qualified SDL.GPU.C as GPU
import qualified SDL.GPU.FC.C as FC

import Control.Monad (when, unless, replicateM_)
import Data.Function (fix, (&))
import Data.Monoid (Sum (..))
import Data.Foldable (for_)
import Data.Word
import GHC.Generics
import Control.Concurrent (threadDelay)
import System.Exit (die, exitFailure)
import System.FilePath
import Foreign.Ptr
import Foreign.C.Types
import Foreign.Marshal.Alloc
import Foreign.Storable
import Foreign.C.String
import SDL.Vect
import qualified SDL
import qualified SDL.GPU.C as GPU
import qualified SDL.GPU.C.Types as Rect (Rect (..))
import qualified SDL.Raw.Types as SDLRaw
import Apecs
import Linear

import Memorable

import LD48.Utils
import LD48.Engine
import LD48.Assets
import qualified LD48.Animation as Animation

data DinoKey
  = DinoKey'Idle
  | DinoKey'Move
  | DinoKey'Kick
  | DinoKey'Hurt
  | DinoKey'Sneak
  deriving stock (Show, Eq, Ord, Bounded, Enum)
  deriving anyclass (Animation.KeyName)

main :: IO ()
main = putStrLn "puhoy there :)"
{-
data PuhoyAssets = PuhoyAssets
  { sounds :: PuhoySounds
  , fonts  :: PuhoyFonts
  , sprites :: PuhoySprites
  , images :: PuhoyImages
  , music :: PuhoyMusic
  } deriving stock Generic
    deriving anyclass Asset

data PuhoyImages = PuhoyImages
  { dino :: Ptr GPU.Image
  , ship :: Ptr GPU.Image
  } deriving stock Generic
    deriving anyclass Asset

data PuhoySounds = PuhoySounds
  { jump :: CuteSound
  , select :: CuteSound
  } deriving stock Generic
    deriving anyclass Asset

data PuhoyMusic = PuhoyMusic
  { famistudio_tutorial :: CuteSound
  } deriving stock Generic
    deriving anyclass Asset

data PuhoyFonts = PuhoyFonts
  { alagard :: Font 40 ('Color 255 255 255 255)
  } deriving stock Generic
    deriving anyclass Asset

data PuhoySprites = PuhoySprites
  { dino :: Animation.Script DinoKey
  } deriving stock Generic
    deriving anyclass Asset

newtype Position = Position (V2 Float) deriving stock Show
instance Component Position where type Storage Position = Map Position

newtype Velocity = Velocity (V2 Float) deriving stock Show
instance Component Velocity where type Storage Velocity = Map Velocity

newtype Clock = Clock Word64
  deriving stock Show
  deriving (Semigroup, Monoid) via (Sum Word64)

instance Component Clock where type Storage Clock = Global Clock

makeWorld "World" [ ''Position, ''Velocity, ''Clock, ''Animation.State ]

main :: IO ()
main = do
  putStrLn "Puhoy there!"

  GPU.setDebugLevel GPU.debugLevelMax

  red <- var (SDLRaw.Color 220 60 1 255)
  white <- var (SDLRaw.Color 255 255 255 255)

  -- TODO: put this in game loop somehow
  --CS.loopSound bgm_sound 1
  --ret <- CS.insertSound ctx bgm_sound
  --when (ret == 0) $ putStrLn "bmg insert_sound ret 0"

  let game = Loop
        { loopEvents = \PuhoyAssets{..} ctx es -> liftIO $ do
            let PuhoySounds{..} = sounds
            let payloads = SDL.eventPayload <$> es
            for_ payloads $ \case
              SDL.MouseButtonEvent e -> when (SDL.mouseButtonEventMotion e == SDL.Pressed) $ do
                SDL.ticks >>= \ticks -> putStrLn $ "beep " ++ show ticks
                replicateM_ 1 $ do
                  let to_play = case SDL.mouseButtonEventButton e of
                        SDL.ButtonLeft -> sound jump
                        _ -> sound select
                  ret' <- CS.insertSound ctx to_play
                  when (ret' == 0) $ putStrLn "insert_sound ret 0"
              _ -> pure ()
        , loopTick = \PuhoyAssets{..} ctx -> do
            -- Tick Animations (use modulus to pick an Enum)
            -- Tick Clock
            pure ()
        , loopDraw = \PuhoyAssets{..} screen -> pure ()

        }
  pure ()

-}
{-
  let loop (n :: Int) anim = do
        let gameLoop = threadDelay 16000 >> loop (n + 1) (Animation.step anim)
        CS.errorReason >>= \err -> unless (err == nullPtr) $ peekCString err >>= \s -> putStrLn ("cute_sound err: " ++ s)
        SDL.pollEvents >>= \es -> do

          CS.mix ctx

          GPU.clear screen
          GPU.circleFilled screen 200 200 200 (toPtr red)
          withCString (show (n `div` 60)) $ \s -> FC.draw nullPtr (rawFont alagard) screen 200 200 s

          Animation.render anim $ \img rect offset -> do
            GPU.setAnchor img 0 0 -- TODO: When to set anchors?
            -- TODO: I think this offset stuff is waaay off
            let v2@(V2 x y) = V2 250 250
            let ov2@(V2 ox oy) = v2 + offset
--            putStrLn $ "offset = " ++ show offset ++ " v2 = " ++ show v2 ++ " ov2 = " ++ show ov2

            allocable $ \outline -> do
              w <- rect *-> Rect.w
              outline & Rect.w *->= w
              h <- rect *-> Rect.h
              outline & Rect.h *->= h
              outline & Rect.x *->= ox
              outline & Rect.y *->= oy

              GPU.rectangle2 screen outline (toPtr white)
              --GPU.blit img rect screen ox oy

              -- Need to do math to rotate around center
              -- but use top-left as image anchor..
              -- TODO: Maybe sdl-gpu should do this? Make an issue
              GPU.blitTransformX img rect screen (ox + w*0.5) (oy + h*0.5) (w*0.5) (h*0.5) 90 1 1

              --GPU.blitRectX img rect screen outline 180 (w*0.5) (h*0.5) GPU.flipVertical
              GPU.circleFilled screen x y 3 (toPtr white)



          GPU.flip screen

          unless (SDL.QuitEvent `elem` payloads) $
            gameLoop

  loop 0 (Animation.init Animation.Loop'Always dino DinoKey'Sneak)
  freeAsset assets
  GPU.quit
-}
