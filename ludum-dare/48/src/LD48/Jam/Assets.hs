{-# LANGUAGE TemplateHaskell #-}

{-# OPTIONS_GHC -ddump-splices -ddump-to-file #-}
module LD48.Jam.Assets where

import GHC.Generics
import qualified SDL.GPU.C.Utils as GPU

-- for th
import Optics.Iso
import Optics.Label
import Optics.Internal.Optic.Types
import Optics.Internal.Magic
import Optics.Lens

import LD48.Assets
import LD48.Utils
import qualified LD48.Animation as Animation

data Assets = Assets
  { animations :: Animations
  , fonts      :: Fonts
  , images     :: Images
  , music      :: Music
  } deriving Generic
    deriving anyclass Asset

data Music = Music
  { bgm :: CuteSound
  } deriving Generic
    deriving anyclass Asset

data Images = Images
  { flashlight_mask :: Ptr GPU.Image
  , flashlight_mask_wide :: Ptr GPU.Image
  , flashlight_mask_wide_dither :: Ptr GPU.Image
  , flashlight_color :: Ptr GPU.Image
  , test_wall :: Ptr GPU.Image
  , enemy_hits :: EnemyHits
  , ghost :: Ptr GPU.Image
  , hand :: Ptr GPU.Image
  , title_screen :: Ptr GPU.Image
  } deriving Generic
    deriving anyclass Asset

data Animations = Animations
  { stairs :: Animation.Script
  , big_ghost :: Animation.Script
  , portraits :: Portraits
  , enemies :: EnemyAnims
  , enemy_lits :: EnemyLits
  , cutscenes :: Cutscenes
  } deriving Generic
    deriving anyclass Asset

data Portraits = Portraits
  { fear1_idle :: Animation.Script
  , fear1_blink :: Animation.Script
  , fear1_gasp :: Animation.Script
  , fear1_look :: Animation.Script
  , fear2_idle :: Animation.Script
  , fear2_blink :: Animation.Script
  , fear2_gasp :: Animation.Script
  , fear2_look :: Animation.Script
  , fear3_idle :: Animation.Script
  , fear3_blink :: Animation.Script
  , fear3_gasp :: Animation.Script
  , fear3_look :: Animation.Script
  , normal_idle :: Animation.Script
  , normal_blink :: Animation.Script
  , normal_gasp :: Animation.Script
  , normal_look :: Animation.Script
  , hit :: Animation.Script
  } deriving Generic
    deriving anyclass Asset

data Cutscenes = Cutscenes
  { middle :: Animation.Script
  , opening :: Animation.Script
  , ending :: Animation.Script
  } deriving Generic
    deriving anyclass Asset

data EnemyAnims = EnemyAnims
  { bat :: Animation.Script
  , creeper :: Animation.Script
  , tentacle :: Animation.Script
  } deriving Generic
    deriving anyclass Asset

data EnemyLits = EnemyLits
  { lit_bat :: Animation.Script
  , lit_creeper :: Animation.Script
  , lit_tentacle :: Animation.Script
  } deriving Generic
    deriving anyclass Asset

data EnemyHits = EnemyHits
  { hit_bat :: Ptr GPU.Image
  , hit_creeper :: Ptr GPU.Image
  , hit_tentacle :: Ptr GPU.Image
  } deriving Generic
    deriving anyclass Asset

data Fonts = Fonts
  { alagard :: Font 16 ('Color 255 255 255 255)
  , alagard__BigRed :: Font 40 ('Color 255 0 0 255)
  , alagard__BigTransparent :: Font 40 ('Color 255 255 255 175)
  } deriving Generic
    deriving anyclass Asset

-- TH_CODE
mkOpticsLabels ''Assets
mkOpticsLabels ''Animations
mkOpticsLabels ''Fonts
mkOpticsLabels ''Images
mkOpticsLabels ''Portraits
mkOpticsLabels ''EnemyAnims
mkOpticsLabels ''EnemyHits
mkOpticsLabels ''Music
