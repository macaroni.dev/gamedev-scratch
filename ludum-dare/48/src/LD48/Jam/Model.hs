{-# LANGUAGE TemplateHaskell #-}

{-# OPTIONS_GHC -ddump-splices -ddump-to-file #-}
module LD48.Jam.Model where

import Data.Word
import Data.Monoid
import Data.Semigroup

import Apecs
import Linear
import CuteC2
import qualified SDL.GPU.C.Utils as GPU
import qualified SDL.GPU.FC.C as FC

import LD48.Assets
import LD48.Frame
import qualified LD48.Animation as Animation

import LD48.Jam.Assets

newtype F1 a b = F1 (a -> b)
instance Show (F1 a b) where
  show _ = "<F1>"

newtype F2 a b c = F2 (a -> b -> c)
instance Show (F2 a b c) where
  show _ = "<F2>"

-- General
newtype Position = Position (V2 Float) deriving stock (Show)
instance Component Position where type Storage Position = Map Position

-- Globals
newtype Clock = Clock Frame
  deriving stock (Show)
  deriving newtype (Enum)
  deriving (Semigroup, Monoid) via (Sum Word64)

instance Component Clock where type Storage Clock = Global Clock

data Scene =
    Gameplay
  | Cutscene Cutscene'
  | PortraitGrid
  | Fade { prev :: Scene, next :: Scene, speed :: Word8, alpha :: Word8 }

data Cutscene' = Cutscene'
  { still :: Maybe (Ptr GPU.Image)
  , anim :: Maybe (Animation.State)
  , text :: Maybe (Ptr FC.Font, Ptr GPU.Rect -> IO (), String)
  , duration :: Maybe Frame
  , waitForDuration :: Bool
  , clickToAdvance :: Bool
  , next :: Scene
  }

newtype CurrentCutscene = CurrentCutscene (Maybe Animation.State)
instance Component CurrentCutscene where type Storage CurrentCutscene = Global CurrentCutscene
instance Semigroup CurrentCutscene where
  a <> _ = a
instance Monoid CurrentCutscene where
  mempty = CurrentCutscene Nothing

instance Semigroup Scene where
  a <> _ = a
instance Monoid Scene where
  mempty = Gameplay
instance Show Scene where
  show = \case
    Gameplay -> "Gameplay"
    Cutscene{} -> "Cutscene"
    PortraitGrid -> "PortraitGrid"
    Fade{} -> "Fade"
instance Component Scene where type Storage Scene = Global Scene


newtype DEBUG = DEBUG { _IS_DEBUG :: Bool }
  deriving stock (Show)
  deriving (Semigroup, Monoid) via (Any)
instance Component DEBUG where type Storage DEBUG = Global DEBUG

-- The player & flashlight
data Player = Player
  deriving stock (Show, Eq, Ord, Enum, Bounded)
  deriving (Semigroup, Monoid) via (Max Player)
instance Component Player where type Storage Player = Global Player

data Portrait = Portrait
  deriving stock (Show, Eq, Ord, Enum, Bounded)
  deriving (Semigroup, Monoid) via (Max Portrait)
instance Component Portrait where type Storage Portrait = Unique Portrait

newtype Depth = Depth Word64
  deriving stock (Show)
  deriving (Semigroup, Monoid) via (Sum Word64)
instance Component Depth where type Storage Depth = Global Depth

newtype Fear = Fear Word64
  deriving stock (Show)
  deriving newtype (Eq, Ord, Enum, Bounded, Num)
  deriving (Semigroup, Monoid) via (Sum Word64)
instance Component Fear where type Storage Fear = Global Fear

data Flashlight = Flashlight Charge deriving stock (Show)
instance Component Flashlight where type Storage Flashlight = Unique Flashlight

data Charge = OutOfBattery | Flickering Frame | Charged Frame | Reloading Frame deriving stock (Show)

data Light = Light deriving stock (Show)
instance Component Light where type Storage Light = Unique Light

-- Enemies

-- What do enemies have?
-- - Type
-- - EnemyState = Windup | Dead | Attack
-- - EnemyClock
-- - Hurtbox
-- - Path = ticks of movement
-- - Position
data Enemy = MrTest | Bat | Tentacle | Creeper deriving stock (Show)
instance Component Enemy where type Storage Enemy = Map Enemy

data EnemyState = Alive [EnemyPlan] | Dead Frame deriving stock (Show)
instance Component EnemyState where type Storage EnemyState = Map EnemyState

data EnemyPlan = EnemyPlan
  { movement :: V2 Float
  , hurtbox  :: Hurtbox
  , attack   :: Maybe Attack
  } deriving stock (Show)

data Attack = Attack
  { fearDmg :: Word64
  } deriving stock (Show)

newtype Hurtbox = Hurtbox { getHurtbox :: [C2Shape] } deriving stock (Show)
instance Component Hurtbox where type Storage Hurtbox = Map Hurtbox

-- The Staircase
data Staircase = Staircase deriving stock (Show)
instance Component Staircase where type Storage Staircase = Unique Staircase


-- TH_CODE
makeWorld "World"
  [ ''Player
  , ''Portrait
  , ''Depth
  , ''Fear
  , ''Flashlight
  , ''Staircase
  , ''Animation.State
  , ''Clock
  , ''Scene
  , ''Enemy
  , ''EnemyState
  , ''Hurtbox
  , ''Position
  , ''Light
  , ''DEBUG
  , ''CurrentCutscene
  ]
