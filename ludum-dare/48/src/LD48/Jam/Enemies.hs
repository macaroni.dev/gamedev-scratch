module LD48.Jam.Enemies where

import Optics
import Control.Monad

import CuteC2
import qualified SDL.GPU.C as GPU

import LD48.Engine
import LD48.Frame
import LD48.Utils
import qualified LD48.Animation as Animation

import LD48.Jam.Model
import LD48.Jam.Assets

-- TODO: Randomness / Inputs
initEnemyPlan :: Enemy -> EnemyPlan
initEnemyPlan = undefined

enemyScript :: Assets -> Enemy -> Animation.Script
enemyScript Assets{..} = \case
  MrTest -> animations ^. #big_ghost
  Bat -> animations ^. #enemies % #bat
  Tentacle -> animations ^. #enemies % #tentacle
  Creeper -> animations ^. #enemies % #creeper

enemyLit :: Assets -> Enemy -> Animation.Script
enemyLit Assets{..} = \case
  MrTest -> animations ^. #big_ghost
  Bat -> animations ^. #enemy_lits % #lit_bat
  Tentacle -> animations ^. #enemy_lits % #lit_tentacle
  Creeper -> animations ^. #enemy_lits % #lit_creeper

enemyHit :: Assets -> Enemy -> (Ptr GPU.Image, Float, Float)
enemyHit Assets{..} = \case
  MrTest -> (images ^. #ghost, 0, 0)
  Bat    -> (images ^. #enemy_hits % #hit_bat, 0, 0)
  Tentacle    -> (images ^. #enemy_hits % #hit_tentacle, 200, 0)
  Creeper    -> (images ^. #enemy_hits % #hit_creeper, 0, 0)

-- Angle + Speed (idk which op but there is one) -> Maybe Length -> [V2]
mkMrTest :: MonadIO m => Assets -> SystemT World m ()
mkMrTest assets@Assets{} = do
  let plan =
        zipWith3 EnemyPlan
        (take 60 (line (degToRad (110)) (repeat (5/60)) Nothing))
        (repeat $ Hurtbox [c2Circle (C2V 30 30) 25])
        (cycle $ reverse $ Just (Attack 10) : replicate 59 Nothing)

  let script = enemyScript assets MrTest
  _ <- newEntity
       ( MrTest
       , Alive plan
       , Position (V2 200 100)
       , Animation.init Animation.Loop'Always script
       )
  pure ()

genEnemies :: MonadIO m => Assets -> SystemT World m ()
genEnemies assets = do
  Clock clk <- gget @Clock
  assets & case clk of
    240 -> genBat1
    420 -> genTentacle
    600 -> genBat2
    620 -> genTentacle
    700 -> genBat1
    701 -> genBat4
    720 -> genTentacle
    721 -> genBat3
    1000 -> genCreeper
    1100 -> genTentacle
    1200 -> genTentacle
    1750 -> genBat3
    1799 -> genBat1
    1800 -> genCreeper
    2000 -> genTentacle
    2100 -> genBat3
    2101 -> genBat4
    2240 -> genBat1
    2420 -> genTentacle
    2600 -> genBat2
    2620 -> genTentacle
    3000 -> genCreeper
    3100 -> genBat2
    3101 -> genBat4

    3199 -> genCreeper
    3200 -> genBat3
    3201 -> genBat4
    3240 -> genBat1

    3400 -> genTentacle
    3401 -> genBat4
    3440 -> genCreeper

    3500 -> genTentacle
    3650 -> genBat1
    3680 -> genBat2
    3700 -> genTentacle
    3701 -> genCreeper

    4000 -> genTentacle
    4060 -> genTentacle
    4120 -> genTentacle
    4121 -> genBat1
    4181 -> genBat3
    4183 -> genBat4
    4184 -> genBat2
    _ -> const (pure ())


bp1 :: Position
bp1 = Position $ V2 200 100
ba1 :: Float
ba1 = 135

bp2 :: Position
bp2 = Position $ V2 400 100
ba2 :: Float
ba2 = 45

genBat4 :: MonadIO m => Assets -> SystemT World m ()
genBat4 = genBat bp2 ba1

genBat3 :: MonadIO m => Assets -> SystemT World m ()
genBat3 = genBat bp2 ba1

genBat2 :: MonadIO m => Assets -> SystemT World m ()
genBat2 = genBat bp2 ba2

genBat1 :: MonadIO m => Assets -> SystemT World m ()
genBat1 = genBat bp1 ba1
genBat :: MonadIO m => Position -> Float -> Assets -> SystemT World m ()
genBat p a' assets = do
  -- Gen location
  -- Angle depends on side of screen
  let plan =
        zipWith3 EnemyPlan
        (take 60 (line (degToRad a') (repeat (10/60)) Nothing))
        (repeat $ Hurtbox [c2Circle (C2V 30 30) 25]) -- TODO: tweak
        (cycle $ replicate 40 Nothing ++ [Just $ Attack 7] ++ repeat (Just $ Attack 0))
  let script = enemyScript assets Bat
  _ <- newEntity
    ( Bat
    , Alive plan
    , p
    , Animation.init (Animation.Loop'Count 0) script
    )
  pure ()

genTentacle :: MonadIO m => Assets -> SystemT World m ()
genTentacle assets = do
  let plan =
        zipWith3 EnemyPlan
        (take 120 (line (degToRad 75) (repeat (1/20)) Nothing))
        (repeat $ Hurtbox [c2Circle (C2V 30 30) 25]) -- TODO: tweak
        (cycle $ replicate 100 Nothing ++ [Just $ Attack 5] ++ repeat (Just $ Attack 0))
  let script = enemyScript assets Tentacle
  _ <- newEntity
    ( Tentacle
    , Alive plan
    , Position (V2 325 75)
    , Animation.init (Animation.Loop'Count 0) script
    )
  pure ()

genCreeper :: MonadIO m => Assets -> SystemT World m ()
genCreeper assets = do
  let path =
        (take 45 (line (degToRad 0) (repeat 0.5) (Just 400))) ++
        (take 45 (line (degToRad 180) (repeat 0.5) (Just 400))) ++
        repeat (V2 0 0)

  let plan =
        zipWith3 EnemyPlan
        path
        (repeat $ Hurtbox [c2Circle (C2V 30 30) 25])
        (replicate 90 Nothing ++ [Just $ Attack 15] ++ replicate 20 (Just $ Attack 0))
  let script = enemyScript assets Creeper
  _ <- newEntity
    ( Creeper
    , Alive plan
    , Position (V2 0 0)
    , Animation.init (Animation.Loop'Always) script
    )
  pure ()

mkPlans
  :: [V2 Float]
  -> [Hurtbox]
  -> [Maybe Attack]
  -> [EnemyPlan]
mkPlans = zipWith3 EnemyPlan

line
  :: Float -- ^ Angle
  -> [Float] -- ^ Velocity
  -> Maybe Float -- ^ Distance traveled
  -> [V2 Float]
line a vs md =
  let av = angle a
      --infLine = iterate (\v2 -> v2 + av * vv) 0
      uncapped = scanl (\v2 v -> v2 + av * pure v) 0 vs
  in case md of
    Nothing -> uncapped
    Just d -> takeWhile (\v2 -> sum v2 <= d) uncapped

destroyEnemy :: MonadIO m => Entity -> SystemT World m ()
destroyEnemy ety = do
  destroyC @Enemy ety
  destroyC @EnemyState ety
  destroyC @Hurtbox ety
  destroyC @Animation.State ety

moveEnemy :: V2 Float -> Position -> Position
moveEnemy v (Position p) = Position (v + p)

enemyDeathLen :: Enemy -> Frame
enemyDeathLen = \case
  _ -> 60
