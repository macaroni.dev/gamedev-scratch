module LD48.Jam.PortraitGrid where

import Optics
import Control.Monad
import Data.Foldable

import qualified SDL
import qualified SDL.GPU.C.Utils as GPU
import qualified SDL.GPU.C.Types as Target (Target (..))
import qualified SDL.GPU.C.Types as Rect (Rect (..))
import Memorable

import qualified LD48.Animation as A
import LD48.Engine
import LD48.Utils
import LD48.Input
import LD48.Jam.Assets
import LD48.Jam.Model
import LD48.Jam.Cutscenes

portraitGridTransition :: MonadEngine Assets m => SystemT World m ()
portraitGridTransition = do
  Portraits{..} <- view (#animations % #portraits) <$> askAssets

  let anims = fmap (A.pad 15 15)
        [ fear1_idle
        , A.pad 10 0 fear1_blink
        , A.pad 0 10 fear1_gasp
        , A.pad 5 5 fear1_look
        , fear2_idle
        , A.pad 7 3 fear2_blink
        , A.pad 3 7 fear2_gasp
        , A.pad 5 5 fear2_look
        , fear3_idle
        , A.pad 12 0 fear3_blink
        , A.pad 0 12 fear3_gasp
        , A.pad 8 4 fear3_look
        , normal_idle
        , A.pad 7 4 normal_blink
        , A.pad 3 8 normal_gasp
        , A.pad 4 6 normal_look
        , hit
        ]
  for_ anims (newEntity . A.init A.Loop'Always)
  prev <- gget @Scene
  gset Fade
    { next = PortraitGrid
    , prev = prev
    , speed = 255
    , alpha = 0
    }

tile :: Ptr GPU.Target -> [A.State] -> IO ()
tile screen = go 0 0 0
  where
    go :: Float -> Float -> Float -> [A.State] -> IO ()
    go tallest currX currY = \case
      [] -> pure ()
      A.State{..} : rest -> do
        screenW <- screen *-> Target.w

        let anims = A.ssAnimations (A.unScript script)
        let img = A.ssImage (A.unScript script)
        let A.SpriteClip{..} = A.currentLocation anims pos
        -- No offsets...
        allocable $ \rect -> do
          rect & Rect.x *->= fromIntegral scX
          rect & Rect.y *->= fromIntegral scY
          rect & Rect.w *->= fromIntegral scW
          rect & Rect.h *->= fromIntegral scH

          let scHf :: Float = fromIntegral scH
          let scWf :: Float = fromIntegral scW
          let won'tFit = currX + scWf > fromIntegral screenW
          let thisX = if won'tFit then 0 else currX
          let thisY = if won'tFit then currY + tallest else currY
          let nextTallest = if won'tFit then scHf else max scHf tallest
          let nextX = thisX + scWf
          let nextY = thisY

          GPU.blit img rect screen thisX thisY
          go nextTallest nextX nextY rest

portraitGridLoop :: Game Assets World
portraitGridLoop =
  Loop
  { loopEvents = \es -> do
      assets <- askAssets
      when (SDL.KeycodeF2 `elem` keypresses es) $ do
        gset Fade
          { next = (Cutscene (titleScreen assets))
          , prev = PortraitGrid
          , speed = 255
          , alpha = 0
          }
  , loopTick = cmap A.step
  , loopDraw = do
      screen <- askGPU
      anims <- cfold (\acc (anim :: A.State, Not :: Not Staircase) -> anim : acc) []
      liftIO $ putStrLn $ "len anim etys " ++ show (length anims)
      liftIO $ tile screen anims
  }
