module LD48.Jam.Cutscenes where

import Optics
import qualified SDL.GPU.C as Rect (Rect (..))
import Memorable

import qualified LD48.Animation as Animation

import LD48.Jam.Model
import LD48.Jam.Assets

openingCutscene :: Assets -> Cutscene'
openingCutscene Assets{..} =
  Cutscene'
  { next = Gameplay
  , still = Nothing
  , anim =
      Just $ Animation.init (Animation.Loop'Count 0) (animations ^. #cutscenes % #opening)
  , text = Nothing
  , duration = Just (5 * 60)
  , waitForDuration = True
  , clickToAdvance = True
  }

middleCutscene :: Assets -> Cutscene'
middleCutscene Assets{..} =
  Cutscene'
  { next = Gameplay
  , still = Nothing
  , anim = Just $ Animation.init (Animation.Loop'Always) (animations ^. #cutscenes % #middle)
  , text = Nothing
  , duration = Just (3 * 60)
  , waitForDuration = True
  , clickToAdvance = True
  }

endingCutscene :: Assets -> Cutscene'
endingCutscene assets@Assets{..} =
  Cutscene'
  { next = Cutscene (titleScreen assets)
  , still = Nothing
  , anim = Just $ Animation.init (Animation.Loop'Count 0) (animations ^. #cutscenes % #ending)
  , text = Nothing
  , duration = Just (10 * 60)
  , waitForDuration = True
  , clickToAdvance = True
  }

titleScreen :: Assets -> Cutscene'
titleScreen assets@Assets{..} =
  Cutscene'
  { next = Cutscene $ openingCutscene assets
  , still = Just (images ^. #title_screen)
  , anim = Nothing
  , text = Nothing
  , duration = Nothing
  , waitForDuration = False
  , clickToAdvance = True
  }

testCutscene :: Assets -> Cutscene'
testCutscene Assets{..} =
  Cutscene'
  { next = Gameplay
  , still = Just (images ^. #test_wall)
  , anim = Nothing
  , text =
      Just
      ( fonts ^. #alagard % #rawFont
      , \rect -> do
          rect & Rect.x *->= 50
          rect & Rect.y *->= 300
          rect & Rect.w *->= 300
          rect & Rect.h *->= 80
      , "Puhoy there!"
      )
  , duration = Just (60 * 10)
  , waitForDuration = False
  , clickToAdvance = True
  }

scaredCutscene :: Assets -> Cutscene'
scaredCutscene Assets{..} =
  Cutscene'
  { next = Gameplay
  , still = Just (images ^. #test_wall)
  , anim = Nothing
  , text =
      Just
      ( fonts ^. #alagard % #rawFont
      , \rect -> do
          rect & Rect.x *->= 50
          rect & Rect.y *->= 300
          rect & Rect.w *->= 300
          rect & Rect.h *->= 80
      , "TOO SCARED???? lmaoooo"
      )
  , duration = Just (60 * 10)
  , waitForDuration = False
  , clickToAdvance = True
  }
