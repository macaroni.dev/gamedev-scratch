module LD48.Utils where

import Control.Monad ((>=>), when, unless)
import Control.Monad.IO.Class
import Control.Monad.IO.Unlift
import Data.Maybe
import Safe
import Debug.Trace
import System.Random

import Foreign.Storable
import Foreign.Marshal.Alloc
import Foreign.Ptr
import Foreign.C.String

import CuteC2
import Linear hiding (trace)

import Optics
import qualified Language.Haskell.TH as TH

import Memorable

allocable :: forall a b m. MemData a => MonadUnliftIO m => (Ptr a -> m b) -> m b
allocable k = do
  UnliftIO{..} <- askUnliftIO
  liftIO $ allocaBytesAligned (memDataSize @a) (memDataAlignment @a) (\p -> unliftIO $ k p)

allocaWith :: forall a b m. Storable a => MonadUnliftIO m => a -> (Ptr a -> m b) -> m b
allocaWith a f = do
  UnliftIO{..} <- askUnliftIO
  liftIO $ alloca $ \ptr -> poke ptr a >> unliftIO (f ptr)

mallocEmpty :: forall a. Storable a => IO (Ptr a)
mallocEmpty = mallocBytes (sizeOf (undefined :: a))

putCStrLn :: CString -> IO ()
putCStrLn = peekCString >=> putStrLn

findMaybe :: (a -> Maybe b) -> [a] -> Maybe b
findMaybe f = headMay . mapMaybe f

(//) :: Integral a => Integral b => a -> b -> Float
a // b = fromIntegral a / fromIntegral b

(.-) :: Ord a => Num a => a -> a -> a
a .- b = if b > a then 0 else a - b

(-!) :: Ord a => Num a => a -> a -> a
a -! b = if b > a then error "(-!) Subtracting below 0!" else a - b

(-.) :: Ord a => Num a => a -> a -> a
a -. b = if b > a then 0 else a - b

traceNamed :: Show a => String -> a -> a
traceNamed n a = trace (n ++ ": " ++ show a) $ a

mkOpticsLabels :: TH.Name -> TH.DecsQ
mkOpticsLabels = Optics.makeFieldLabelsWith Optics.noPrefixFieldLabels

c2v2v2 :: C2V -> V2 Float
c2v2v2 (C2V x y) = V2 x y

v22c2v :: V2 Float -> C2V
v22c2v (V2 x y) = C2V x y

-- Taken from Graphics.Gloss.Geometry.Angle
-- | Convert degrees to radians
degToRad :: Float -> Float
degToRad d      = d * pi / 180
{-# INLINE degToRad #-}


-- | Convert radians to degrees
radToDeg :: Float -> Float
radToDeg r      = r * 180 / pi
{-# INLINE radToDeg #-}


-- | Normalize an angle to be between 0 and 2*pi radians
normalizeAngle :: Float -> Float
normalizeAngle f = f - 2 * pi * floor' (f / (2 * pi))
 where  floor' :: Float -> Float
        floor' x = fromIntegral (floor x :: Int)
{-# INLINE normalizeAngle #-}

randomChoice :: MonadIO m => [a] -> m a
randomChoice as = do
  i <- randomRIO (0, length as - 1)
  pure (as !! i)
