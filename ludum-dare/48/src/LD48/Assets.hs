{-# LANGUAGE TemplateHaskell #-}

{-# OPTIONS_GHC -ddump-splices -ddump-to-file #-}
module LD48.Assets
  ( module LD48.Assets
  , module Foreign.Ptr
  ) where

import Foreign.Ptr
import Foreign.Storable
import Foreign.Marshal.Alloc
import Foreign.C.String
import System.FilePath
import System.Environment
import Data.List.Split

import Control.Monad (when, unless)
import GHC.TypeLits
import GHC.Generics
import Data.Kind
import Data.Proxy
import Data.Aeson (FromJSON)

-- for th
import Optics.Iso
import Optics.Label
import Optics.Internal.Optic.Types
import Optics.Internal.Magic
import Optics.Lens

import qualified Cute.Sound.C as CS
import qualified SDL.Raw.Types as SDLRaw
import qualified SDL.GPU.C as GPU
import qualified SDL.GPU.FC.C as FC
import Memorable

import LD48.Utils
import LD48.Frame
import qualified LD48.Animation as Animation

data Color = Color Nat Nat Nat Nat

type family KN (ns :: [Nat]) :: Constraint where
  KN '[] = ()
  KN (n ': ns) = (KnownNat n, KN ns)

-- TODO?: Make this take a list of configs that we fold over
newtype Font (size :: Nat) (color :: Color) = Font { rawFont :: (Ptr FC.Font) }

floatVal :: forall n. KnownNat n => Float
floatVal = fromIntegral (natVal (Proxy :: Proxy n))

intgVal :: forall n a. KnownNat n => Integral a => a
intgVal = fromIntegral (natVal (Proxy :: Proxy n))

class Asset a where
  loadAsset :: FilePath -> IO a
  default loadAsset :: (Generic a, GAsset (Rep a)) => FilePath -> IO a
  loadAsset path = to <$> gloadAsset path

  freeAsset :: a -> IO ()
  default freeAsset :: (Generic a, GAsset (Rep a)) => a -> IO ()
  freeAsset = gfreeAsset . from

-- Assumes png atm
-- TODO: Filetype discovery
instance Asset (Ptr GPU.Image) where
  loadAsset path = do
    withCString (path <.> "png") $ \p -> do
      img <- GPU.loadImage p
      when (img == nullPtr) $ error $ "Could not load " ++ path <.> "png"
      GPU.setAnchor img 0 0
      pure img
  freeAsset = GPU.freeImage

instance (KN '[size, r, g, b, a]) => Asset (Font size ('Color r g b a)) where
  loadAsset path = do
    font <- FC.createFont
    let realPath = head $ splitOn "__" path
    ret <- withCString (realPath <.> "ttf") $ \p -> alloca $ \c -> do
      poke c (SDLRaw.Color (intgVal @r) (intgVal @g) (intgVal @b) (intgVal @a))
      FC.loadFont font p (intgVal @size) c 0
    unless (GPU.isTrue ret) $ error ("Failed to load " ++ realPath <.> "ttf")
    pure $ Font font
  freeAsset = FC.freeFont . rawFont

instance Asset (Ptr CS.LoadedSound) where
  loadAsset path = do
    sound <- mallocEmpty @CS.LoadedSound
    withCString (path <.> "wav") $ \p -> CS.loadWav sound p
    CS.errorReason >>= \s -> unless (s == nullPtr) $ do
      err <- peekCString s
      error $ "Failed to load " ++ path <.> "wav" ++ " due to " ++ err
    pure sound

  freeAsset s = CS.freeSound s >> free s

instance Asset CS.PlaySoundDef where
  loadAsset path = do
    loaded <- loadAsset @(Ptr CS.LoadedSound) path
    alloca $ \ptr -> CS.makeDef ptr loaded >> peek ptr

  freeAsset CS.PlaySoundDef{loaded} = freeAsset loaded

-- TODO: Phantoms for config (e.g. loop)
data CuteSound = CuteSound { loaded :: Ptr CS.LoadedSound, sound :: Ptr CS.PlayingSound }

instance Asset CuteSound where
  loadAsset path = do
    loaded <- loadAsset @(Ptr CS.LoadedSound) path
    sound <- mallocEmpty @CS.PlayingSound
    CS.makePlayingSound sound loaded
    pure CuteSound{..}

  freeAsset CuteSound{..} = CS.stopSound sound >> freeAsset loaded >> free sound

instance Asset Animation.Script where
  loadAsset path = Animation.Script <$> Animation.readSpriteSheetYAML load (path <.> "yaml")
    where
      load _ (Just _) = error "Transparency color not supported"
      load fp Nothing =
        let fullPath = takeDirectory path </> fp
         in withCString fullPath $ \p -> do
          img <- GPU.loadImage p
          when (img == nullPtr) $ error $ "Could not load " ++ fullPath
          pure img

  freeAsset (Animation.Script Animation.SpriteSheet{..}) = GPU.freeImage ssImage

-- Generic deriving: Use records to reflect your assets' directory structure
class GAsset a where
  gloadAsset :: FilePath -> IO (a x)
  gfreeAsset :: a x -> IO ()

instance (GAsset a, GAsset b) => GAsset (a :*: b) where
  gloadAsset path = (:*:) <$> gloadAsset path <*> gloadAsset path
  gfreeAsset (a :*: b) = gfreeAsset a >> gfreeAsset b

instance GAsset a => GAsset (D1 _1 a) where
  gloadAsset path = M1 <$> gloadAsset path
  gfreeAsset (M1 a) = gfreeAsset a

instance GAsset a => GAsset (C1 _1 a) where
  gloadAsset path = M1 <$> gloadAsset path
  gfreeAsset (M1 a) = gfreeAsset a

instance (Asset asset, KnownSymbol name) =>
  GAsset (S1 ('MetaSel ('Just name) _1 _2 _3)
          (K1 _4 asset)) where
  gloadAsset path = (M1 . K1) <$> loadAsset (path </> (symbolVal (Proxy @name)))
  gfreeAsset (M1 (K1 asset)) = freeAsset asset

----

getAssetPrefix :: IO FilePath
getAssetPrefix = lookupEnv "USE_PWD_ASSETS" >>= \case
  Just "1" -> pure ""
  _ -> takeDirectory <$> getExecutablePath

loadAssets :: forall a. Asset a => FilePath -> IO a
loadAssets dir = do
  prefix <- getAssetPrefix
  loadAsset (prefix </> dir)

-- TH_CODE
mkOpticsLabels ''Font
mkOpticsLabels ''CuteSound
