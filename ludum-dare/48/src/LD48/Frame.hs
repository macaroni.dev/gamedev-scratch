module LD48.Frame where

import Data.Word
import Data.Aeson (FromJSON)

newtype Frame = Frame Word64
  deriving stock (Show)
  deriving newtype (Eq, Ord, Enum, Bounded, Num, Integral, Real, FromJSON)
