module LD48.Animation
  ( module LD48.Animation
  -- * Re-exports
  , A.KeyName (..)
  , A.SpriteSheet (..)
  , A.SpriteClip (..)
  , A.Loop (..)
  , A.Animations(..)
  , A.readSpriteSheetYAML
  , A.currentLocation
  ) where

import qualified Animate as A

import Data.Function((&))
import Data.Maybe (fromMaybe)
import qualified Data.Vector as Vector

import Foreign.Ptr
import Foreign.Marshal.Alloc
import qualified SDL.GPU.C as GPU
import qualified SDL.GPU.C.Types as Rect (Rect (..))
import Memorable
import Linear
import Apecs

import LD48.Frame
import LD48.Utils

data The'Animation = The'Animation
  deriving stock (Show, Eq, Ord, Bounded, Enum)
  deriving anyclass (A.KeyName)

newtype Script = Script { unScript ::  (A.SpriteSheet The'Animation (Ptr GPU.Image) Frame) }

data State = State
  { script :: Script
  , pos    :: A.Position The'Animation Frame
  , paused :: Bool
  , clock  :: Frame
  }
-- TODO: Pause, Ptr GPU.Image -> IO () for tweaks, etc

-- TODO: Unsure if making this a Component really helps
-- - Should pos be a list to allow for an entity to be made of multiple animations? Or should the storage allow for it? Does it matter?
-- - How can an entity "cancel" one animation but not the other?
-- - We could use Typeable?

instance Component State where type Storage State = Map State

init :: A.Loop -> Script -> State
init loop s =
  State
  { script = s
  , pos = A.initPositionWithLoop The'Animation loop
  , paused = False
  , clock = 0
  }

pad :: Frame -> Frame -> Script -> Script
pad front back (Script as) =
  let fs  = (A.unAnimations $ A.ssAnimations as) Vector.! 0
      f0  = fs Vector.! 0
      n   = length fs - 1
      fN  = fs Vector.! n
      us  =
        [ ( 0 , f0 { A.fDelay = A.fDelay f0 + front })
        , ( n , fN { A.fDelay = A.fDelay fN + back })
        ]
      fs' = fs Vector.// us
  in Script $ as { A.ssAnimations = A.Animations $ pure fs' }

switch :: A.Loop -> Script -> State -> State
switch loop newScript State{..} =
  State
  { script = newScript
  , pos = A.stepPosition
          (A.ssAnimations (unScript newScript))
          (A.initPositionWithLoop The'Animation loop)
          clock
  , clock = clock
  , paused = paused
  }
pause :: State -> State
pause s = s { paused = True }

unpause :: State -> State
unpause s = s { paused = False }

step :: State -> State
step s@State{..} =
  let anims = A.ssAnimations (unScript script)
   in if paused then s else State { script = script, pos = A.stepPosition anims pos 1, clock = clock + 1, .. }

isDone :: State -> Bool
isDone State{..} =
    let anims = A.ssAnimations (unScript script)
    in A.isAnimationComplete anims pos

-- TODO: This offset nonsense...use animate-sdl as reference
-- Retu
render :: Num a => State -> (Ptr GPU.Image -> Ptr GPU.Rect -> V2 a -> IO r) -> IO r
render State{..} k = do
  let anims = A.ssAnimations (unScript script)
  let A.SpriteClip{..} = A.currentLocation anims pos
  -- Lifted from animate-sdl2
  -- TODO: Still not sure how these offsets will work w/sdl-gpu..gotta test!
  -- TODO: I think this offset stuff is waaay off
  let vOffset = fromMaybe
        (V2 0 0)
        ((\(x,y) -> fromIntegral <$> V2 (-x) (-y)) <$> scOffset)
  allocable $ \rect -> do
    rect & Rect.x *->= fromIntegral scX
    rect & Rect.y *->= fromIntegral scY
    rect & Rect.w *->= fromIntegral scW
    rect & Rect.h *->= fromIntegral scH
    k (A.ssImage (unScript script)) rect vOffset
