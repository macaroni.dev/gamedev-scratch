{-# OPTIONS_GHC -fno-warn-orphans #-}
module LD48.Engine
  ( module LD48.Engine
  , module Apecs
  , module Foreign.Ptr
  , module Foreign.C.String
  , module Foreign.Storable
  , module Foreign.Marshal.Alloc
  , module Linear.Affine
  , module Linear
  , module Control.Monad.IO.Unlift
  ) where

import Control.Monad
import Control.Applicative
import Control.Monad.IO.Unlift
import Control.Exception
import Data.Word
import Data.Foldable
import Data.Function
import Foreign.Ptr
import Foreign.C.String
import Foreign.Storable
import Foreign.Marshal.Alloc
import Control.Monad.Managed
import Control.Monad.Reader
import System.Mem (performGC)

import Apecs hiding (Map)
import Apecs.Core
import qualified SDL
import qualified Cute.Sound.C as CS
import qualified SDL.GPU.C.Utils as GPU
import qualified SDL.GPU.C as Target (Target(..))
import Memorable
import Linear
import Linear.Affine

import LD48.Assets
import LD48.Input
import LD48.Utils

{-
gameLoop abstraction
- Init sdl-gpu
- Init cute_sound
- Init apecs
- Load assets
- Start Loop
  - pollEvents
  - processEvents :: [SDL.Event] -> System w m ()
  - runSystems :: assets -> CS.Context -> System w m ()
  - GPU.clear
  - render :: assets -> GPU.Target -> System w m ()
  - GPU.flip
  - cs_mix()
  - Sleep to make it 60fps (taking into account exec time)
- Free resources
-}

gameWidth :: Word16
gameHeight :: Word16
gameWidth = 640
gameHeight = 400
gameAspect :: Float
gameAspect = (fromIntegral gameWidth) / (fromIntegral gameHeight)

class MonadUnliftIO m => MonadEngine assets m | m -> assets where
  askAssets :: m assets
  askSound  :: m (Ptr CS.Context)
  askGPU    :: m (Ptr GPU.Target)

data Env assets = Env
  { envAssets :: assets
  , envSound  :: Ptr CS.Context
  , envGPU    :: Ptr GPU.Target
  }

instance MonadEngine assets (ReaderT (Env assets) IO) where
  askAssets = asks envAssets
  askSound  = asks envSound
  askGPU    = asks envGPU


instance MonadEngine assets m => MonadEngine assets (SystemT w m) where
  askAssets = lift askAssets
  askSound  = lift askSound
  askGPU    = lift askGPU

-- Orphan
instance MonadUnliftIO m => MonadUnliftIO (SystemT w m) where
  withRunInIO = wrappedWithRunInIO SystemT unSystem

data Game assets w =
  Loop
  { loopEvents :: forall m. MonadEngine assets m => [SDL.Event] -> SystemT w m ()
  , loopTick :: forall m. MonadEngine assets m => SystemT w m ()
  , loopDraw :: forall m. MonadEngine assets m => SystemT w m ()
  }

-- TODO:
-- - Give caller control over resolution, full-screen, virtual resolution
--   - and/or give some utils to help..maybe a function passed in?
--     Or we just hard-code it here..
--     Letterboxing: https://github.com/grimfang4/sdl-gpu/issues/188
-- - Add an mtl interface around assets, sound, and screen
runGame
  :: forall assets w
   . Asset assets
  => FilePath
  -> (assets -> Ptr GPU.Target -> Ptr CS.Context -> IO w) -- ^ init
  -> (forall m. MonadEngine assets m => SystemT w m (Game assets w)) -- ^ FINALLY
  -> IO ()
runGame assetPrefix initialize pickLoop = runManaged $ do
  liftIO $ GPU.setDebugLevel GPU.debugLevelMax

  let ticksPerFrame :: Word32 = ceiling (1000 / 60 :: Double)
  --liftIO $ putStrLn $ "ticksPerFrame = " ++ show ticksPerFrame

  screen <- managed (bracket (GPU.init gameWidth gameHeight GPU.defaultInitFlags) (\_ -> GPU.quit))
  cs_ctx <- managed (bracket (CS.makeContext nullPtr 44100 15 0 nullPtr) CS.shutdownContext)

  liftIO $ GPU.setVirtualResolution screen gameWidth gameHeight
  liftIO $ GPU.setDefaultAnchor 0 0

  -- NOTE: We must load assets LAST because we need things initialized
  assets <- managed (bracket (loadAssets @assets assetPrefix) freeAsset)

  SDL.cursorVisible SDL.$= False

  world <- liftIO $ initialize assets screen cs_ctx

  liftIO $ fix $ \loop -> do
    startTicks <- SDL.ticks
    events <- SDL.pollEvents

    for_ (keypresses events) $ \case
      SDL.KeycodeF11 -> do
        isFullscreen <- GPU.toggleFullscreen Nothing gameWidth gameHeight screen
        _ <- fmap unwords $ sequence $
          [ pure "isFullScreen", pure $ show isFullscreen
          , pure "w =", show <$> screen *-> Target.w
          , pure "h =", show <$> screen *-> Target.h
          , pure "base_w =", show <$> screen *-> Target.base_w
          , pure "base_h =", show <$> screen *-> Target.base_h
          ]

        pure ()
      _ -> pure ()

    let env = Env
          { envAssets = assets
          , envSound  = cs_ctx
          , envGPU    = screen
          }

    flip runReaderT env $ Apecs.runWith world $ do
      Loop{..} <- pickLoop
      loopEvents events

      loopTick

      liftIO $ GPU.clear screen
      loopDraw
      liftIO $ GPU.flip screen
    CS.mix cs_ctx
    performGC
    endTicks <- SDL.ticks
    let elapsedTicks = (endTicks - startTicks)
    let remainingTicks = if (ticksPerFrame > elapsedTicks) then ticksPerFrame - elapsedTicks else 0

    -- TODO: Spin Lock / use getPerformanceCounter for more
    -- accurate & consistent fps limiting
    -- TODO: Validate that this FPS limiting is working
    -- TODO: Step-debugging (gated behind env var)
    if remainingTicks > 0
      then SDL.delay remainingTicks
      else pure ()--putStrLn "Framerate drop"

    unless (SDL.QuitEvent `elem` fmap SDL.eventPayload events || SDL.KeycodeEscape `elem` keypresses events) loop

gset
  :: (Has w m c, Apecs.Core.ExplSet m (Storage c))
  => c -> SystemT w m ()
gset = Apecs.set global

gget
  :: forall c w m
   . (Has w m c, Apecs.Core.ExplGet m (Storage c))
  => SystemT w m c
gget = Apecs.get global

ggets
  :: forall c r w m
   . (Has w m c, Apecs.Core.ExplGet m (Storage c))
  => (c -> r)
  -> SystemT w m r
ggets f = f <$> gget

gmodify
  :: forall c w m
   . (Apecs.Core.ExplGet m (Storage c), Has w m c
     , Apecs.Core.ExplSet m (Storage c))
  => (c -> c)
  -> SystemT w m ()
gmodify = Apecs.modify global


existsC
  :: forall c w m. (Has w m c, Apecs.Core.ExplGet m (Storage c))
  => Entity
  -> SystemT w m Bool
existsC ety = exists ety (Proxy @c)

destroyC
  :: forall c w m
   . (Has w m c, Apecs.Core.ExplDestroy m (Storage c))
  => Entity
  -> SystemT w m ()
destroyC e = destroy e (Proxy @c)

cfoldMap
  :: forall w m c a
   . Members w m c
  => Get w m c
  => Monoid a
  => (c -> a)
  -> SystemT w m a
cfoldMap f = cfold (\acc c -> mappend acc (f c)) mempty

cfoldMapM
  :: forall w m c a
   . Members w m c
  => Get w m c
  => Monoid a
  => (c -> SystemT w m a)
  -> SystemT w m a
cfoldMapM f = cfoldM (\acc c -> mappend <$> pure acc <*> f c) mempty

infixr 2 .||.
(.||.) :: Applicative f => f Bool -> f Bool -> f Bool
(.||.) = liftA2 (||)

infixr 3 .&&.
(.&&.) :: Applicative f => f Bool -> f Bool -> f Bool
(.&&.) = liftA2 (&&)
