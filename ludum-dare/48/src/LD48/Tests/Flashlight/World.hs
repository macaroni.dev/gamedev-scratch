{-# LANGUAGE TemplateHaskell #-}

{-# OPTIONS_GHC -ddump-splices -ddump-to-file #-}

-- TH isolation
module LD48.Tests.Flashlight.World where

import Foreign.Ptr

import Apecs
import Linear
import qualified SDL.GPU.C.Utils as GPU

import qualified LD48.Animation as Animation

data Flashlight = Flashlight deriving stock Show
instance Component Flashlight where type Storage Flashlight = Unique Flashlight

data Staircase = Staircase deriving stock Show
instance Component Staircase where type Storage Staircase = Unique Staircase

newtype Position = Position (V2 Float) deriving stock Show
instance Component Position where type Storage Position = Map Position

newtype Image = Image (Ptr GPU.Image) deriving stock Show
instance Component Image where type Storage Image = Map Image

-- TH_CODE
makeWorld "World" [''Flashlight, ''Position, ''Staircase, ''Animation.State, ''Image ]
