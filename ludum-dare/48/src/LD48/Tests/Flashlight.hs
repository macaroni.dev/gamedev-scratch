{-# LANGUAGE NoMonomorphismRestriction #-}

{-# OPTIONS_GHC -Wno-all #-}
module LD48.Tests.Flashlight where

import Control.Monad
import Foreign.Ptr
import Foreign.Storable
import Foreign.Marshal.Alloc
import GHC.Generics

import Apecs
import qualified SDL.GPU.C.Utils as GPU
import qualified SDL.Utils as SDL

import Linear
import Linear.Affine

import LD48.Utils
import LD48.Assets
import LD48.Engine
import LD48.Input
import qualified LD48.Animation as Animation

import LD48.Tests.Flashlight.World

data Assets = Assets
  { dark_wall :: Ptr GPU.Image
  , light_wall :: Ptr GPU.Image
  , flashlight_mask :: Ptr GPU.Image
  , flashlight_mask2 :: Ptr GPU.Image
  , flashlight_mask_dither8 :: Ptr GPU.Image
  , flashlight_mask_dither4 :: Ptr GPU.Image
  , testmonster1 :: Ptr GPU.Image
  , ghost :: Ptr GPU.Image
  , ld48_test :: Animation.Script
  , stairs :: Animation.Script
  } deriving Generic
    deriving anyclass Asset

main :: IO ()
main = runGame @Assets @World "assets/flashlight_test" initialize (pure loop)
  where
    initialize Assets{..} _ _ = do
      putStrLn "here comes the anims"
      putStrLn $ "anims = " ++ show (Animation.ssAnimations $ Animation.unScript ld48_test)

      --GPU.generateMipmaps ld48_test1_0000

      GPU.centerAnchor flashlight_mask
      GPU.centerAnchor flashlight_mask_dither8
      GPU.centerAnchor flashlight_mask_dither4
      GPU.centerAnchor flashlight_mask2
{-
      GPU.setBlending dark_wall 1
      GPU.setBlending light_wall 1
      GPU.setBlending flashlight_mask 1
      GPU.setBlending flashlight_mask_dither8 1
      GPU.setBlending flashlight_mask_dither4 1
      GPU.setBlending flashlight_mask2 1-}

      GPU.setBlendFunction flashlight_mask GPU.funcZero GPU.funcOne GPU.funcDstColor GPU.funcZero
      GPU.setBlendFunction flashlight_mask_dither8 GPU.funcZero GPU.funcOne GPU.funcDstColor GPU.funcZero
      GPU.setBlendFunction flashlight_mask_dither4 GPU.funcZero GPU.funcOne GPU.funcDstColor GPU.funcZero

      GPU.setBlendFunction dark_wall GPU.funcDstAlpha GPU.funcOneMinusDstAlpha GPU.funcDstAlpha GPU.funcOneMinusDstAlpha

      world <- initWorld
      runWith world $ do
        _ <- newEntity (Flashlight, Position 0, Image flashlight_mask_dither4)
        _ <- newEntity (Staircase, Position 0, Animation.init Animation.Loop'Always stairs)
        pure ()

      pure world

    loop :: Game Assets World = Loop
      { loopEvents = \es -> do
          Assets{..} <- askAssets
          cs_ctx <- askSound
          screen <- askGPU
          (P (V2 ax ay)) <- SDL.getAbsoluteMouseLocation
          --liftIO $ putStrLn $ "ax = " ++ show ax ++ " ay = " ++ show ay

          (vx, vy) <- liftIO $ alloca $ \vxp -> alloca $ \vyp -> do
            GPU.getVirtualCoords screen vxp vyp (fromIntegral ax) (fromIntegral ay)
            (,) <$> peek vxp <*> peek vyp

          cmap $ \Flashlight -> Position (V2 vx vy)

          when (wasMousePressed SDL.ButtonLeft es) $
            cmap $ \Flashlight -> Image flashlight_mask_dither4
          when (wasMousePressed SDL.ButtonRight es) $
            cmap $ \Flashlight -> Image flashlight_mask_dither8
          pure ()

      , loopTick   = do
          Assets{..} <- askAssets
          cs_ctx <- askSound
          cmap Animation.step
          pure ()
      , loopDraw   = do
          Assets{..} <- askAssets
          screen <- askGPU
          -- - Lit wall is in the way back
          -- - The dark wall is in the front
          -- - In between, we have a mask that blends to turn part of the
          --   dark fully transparent, revealing the light below
--          liftIO $ GPU.blit light_wall nullPtr screen 0 0

--          cmapM_ $ \(Flashlight, (Position (V2 x y))) -> do
--            liftIO $ GPU.blit flashlight_mask_dither4 nullPtr screen x y

--          liftIO $ GPU.blit dark_wall nullPtr screen 0 0

          cmapM_ $ \(Staircase, (Position p), anim@Animation.State{}) -> do
            cmapM_ $ \(Flashlight, (Position (V2 x y)), (Image mask)) -> do
              liftIO $ Animation.render anim $ \img rect offset -> do
                GPU.setAnchor img 0 0 -- TODO: Set these in LD48.Assets!
                let ov2@(V2 ox oy) = p + offset

                -- The light is in the far back (hidden)
                GPU.blit img rect screen ox oy

                -- The mask will reveal it
                GPU.blit mask nullPtr screen x y

                -- Now blit the dark one
                GPU.withBlendFunc img
                  GPU.funcDstAlpha
                  GPU.funcOneMinusDstAlpha
                  GPU.funcDstAlpha
                  GPU.funcOneMinusDstAlpha $
                  GPU.withColor img (GPU.Color 55 28 82 255) $
                  GPU.blit img rect screen ox oy



          liftIO $ GPU.blit ghost nullPtr screen 300 300

          liftIO $ GPU.blit testmonster1 nullPtr screen 300 100

          cmapM_ $ \(Flashlight, (Position (V2 x y))) -> do
            liftIO $ allocaWith (GPU.Color 255 0 0 255) $ \red -> do
              GPU.line screen (x-5) (y-5) (x+5) (y+5) red
              GPU.line screen (x-5) (y+5) (x+5) (y-5) red

          --liftIO $ GPU.blit ld48_test1_0000 nullPtr screen 0 0
          pure ()
      }
