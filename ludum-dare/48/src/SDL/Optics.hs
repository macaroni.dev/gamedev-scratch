{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FunctionalDependencies #-}

{-# OPTIONS_GHC -ddump-splices -ddump-to-file #-}

module SDL.Optics where

import Data.Int
import Data.Word
import Optics
import SDL

import SDL.Optics.Rules

-- TH_CODE
makePrisms ''SDL.EventPayload

makeLensesWith rules ''SDL.Event

makeLensesWith rules ''SDL.KeyboardEventData

makeLensesWith rules ''SDL.Keysym

makeLensesWith rules ''SDL.MouseMotionEventData

makeLensesWith rules ''SDL.MouseButtonEventData

makeLensesWith rules ''SDL.MouseWheelEventData
