module SDL.GPU.C.Utils
  ( module SDL.GPU.C.Utils
  , module GPU
  , Color (..)
  ) where

import Prelude hiding (not)

import Control.Monad
import Control.Monad.IO.Unlift
import UnliftIO.Exception
import Data.Word
import Data.Function

import Foreign.Ptr
import Foreign.Marshal.Alloc

import SDL.GPU.C as GPU
import SDL.GPU.C as GPU.Image (Image (..))
import SDL.GPU.C as GPU.Target (Target (..))
import SDL.GPU.C as GPU.Rect (Rect (..))
import SDL.Raw.Types (Color (..))
import Memorable

import LD48.Utils

withBlendFunc
  :: MonadUnliftIO m
  => Ptr GPU.Image
  -> GPU.BlendFuncEnum
  -> GPU.BlendFuncEnum
  -> GPU.BlendFuncEnum
  -> GPU.BlendFuncEnum
  -> m r
  -> m r
withBlendFunc img bf1 bf2 bf3 bf4 ior =
  bracket_ (liftIO $ GPU.setBlendFunction img bf1 bf2 bf3 bf4) (liftIO $ GPU.setBlendMode img GPU.blendNormal) ior

withBlendMode
  :: MonadUnliftIO m
  => Ptr Image
  -> BlendPresetEnum
  -> m r
  -> m r
withBlendMode img mode ior =
  bracket_ (liftIO $ GPU.setBlendMode img mode) (liftIO $ GPU.setBlendMode img GPU.blendNormal) ior

withColor
  :: MonadUnliftIO m
  => Ptr GPU.Image
  -> Color
  -> m r
  -> m r
withColor img color ior = allocaWith color $ \pc ->
  bracket_ (liftIO $ GPU.setColor img pc) (liftIO $ GPU.unsetColor img) ior

withScreenColor
  :: MonadUnliftIO m
  => Ptr GPU.Target
  -> Color
  -> m r
  -> m r
withScreenColor screen color ior = allocaWith color $ \pc ->
  bracket_ (liftIO $ GPU.setTargetColor screen pc) (liftIO $ GPU.unsetTargetColor screen) ior

-- Anchor is between 0.0 and 1.0
centerAnchor :: Ptr GPU.Image -> IO ()
centerAnchor img = GPU.setAnchor img 0.5 0.5

not :: GPU.Bool -> GPU.Bool
not b = if GPU.isTrue b then 0 else 1

-- https://github.com/grimfang4/sdl-gpu/issues/188
toggleFullscreen :: Maybe GPU.Bool -> Word16 -> Word16 -> Ptr GPU.Target -> IO GPU.Bool
toggleFullscreen mForce virtW virtH screen = do
  current <- GPU.getFullscreen
  isNowFull <- case mForce of
    Nothing -> GPU.setFullscreen (not current) 1
    Just b -> GPU.setFullscreen b 1

  when (GPU.isTrue isNowFull) $ do
    let !aspect = virtW // virtH
    newW <- screen *-> GPU.Target.base_w
    newH <- screen *-> GPU.Target.base_h

    let !scaleW = newW // virtW
    let !scaleH = newH // virtH

    allocable @GPU.Rect $ \rect -> do
      if scaleW < scaleH then do
        let !rectH = fromIntegral newW / aspect
        rect & GPU.Rect.w *->= fromIntegral newW
        rect & GPU.Rect.h *->= rectH
        rect & GPU.Rect.x *->= 0
        rect & GPU.Rect.y *->= ((fromIntegral newH - rectH)/2)
        else do
        let !rectW = fromIntegral newH * aspect
        rect & GPU.Rect.w *->= rectW
        rect & GPU.Rect.h *->= fromIntegral newH
        rect & GPU.Rect.x *->= ((fromIntegral newW - rectW)/2)
        rect & GPU.Rect.y *->= 0
      GPU.setViewport screen rect
  pure isNowFull
