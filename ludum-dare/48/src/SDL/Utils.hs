{-# OPTIONS_GHC -Wno-all #-}

module SDL.Utils
( module SDL.Utils
, module SDL.Optics
, module SDL
) where

import qualified Data.Set
import SDL
import SDL.Optics

functionKeycodes :: Data.Set.Set Keycode
functionKeycodes = Data.Set.fromList
  [ SDL.KeycodeF1
  , SDL.KeycodeF2
  , SDL.KeycodeF3
  , SDL.KeycodeF4
  , SDL.KeycodeF5
  , SDL.KeycodeF6
  , SDL.KeycodeF7
  , SDL.KeycodeF8
  , SDL.KeycodeF9
  , SDL.KeycodeF10
  , SDL.KeycodeF11
  , SDL.KeycodeF12
  ]
