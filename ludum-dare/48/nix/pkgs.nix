let
  sources = import ./sources.nix {};
  haskellNix = import sources.haskellNix {};
  cmake = import ./overlays/cmake.nix;
  rhash = import ./overlays/rhash.nix;
  SDL2 = import ./overlays/SDL2.nix;
  SDL2_ttf = import ./overlays/SDL2_ttf.nix;
  freetype = import ./overlays/freetype.nix;
  SDL_gpu = import ./overlays/SDL_gpu.nix;
  SDL_FontCache = import ./overlays/SDL_FontCache.nix;
  fftw = import ./overlays/fftw.nix;
  libdrm = import ./overlays/libdrm.nix;
  pkgs = import
    haskellNix.sources.nixpkgs-2009
    {
      config = haskellNix.nixpkgsArgs.config;
      system = haskellNix.nixpkgsArgs.system;
      # TODO: Remove cmake/rhash?
      overlays = haskellNix.nixpkgsArgs.overlays ++ [ cmake rhash.patch SDL2.x11 SDL2.noPatch SDL_gpu SDL2_ttf SDL_FontCache freetype fftw libdrm ];
    };
in
pkgs
