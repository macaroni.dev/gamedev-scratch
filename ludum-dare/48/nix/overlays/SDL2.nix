# Try composeManyExtensions instead?
{
  x11 = self: super:
    {
      SDL2 = super.SDL2.override ({
        x11Support = !self.stdenv.hostPlatform.isWindows && !self.stdenv.isCygwin && !self.stdenv.hostPlatform.isAndroid;
      });
    };
  noPatch = self: super:
    {
      SDL2 = super.SDL2.overrideAttrs (old: if !self.stdenv.hostPlatform.isWindows then {} else {
        postPatch = "";
        postInstall = ''
          rm $out/lib/*.la
          moveToOutput bin/sdl2-config "$dev"
        '';
      });
    };
}
