self: super:
{
  fftw = super.fftw.overrideAttrs(old: if !self.stdenv.hostPlatform.isMusl then {} else {
    configureFlags = self.lib.lists.remove "--enable-openmp" old.configureFlags;
  });
}
