self: super:
let sdlFC = { lib, stdenv, fetchFromGitHub, cmake, SDL2, SDL2_ttf, SDL_gpu }:
    let gh_src = fetchFromGitHub {
          owner = "ramirez7";
          repo = "SDL_FontCache";
          rev = "7fa7da4b6664e60c037bf40df131b04cc4be6688";
          sha256 = "16p71mkbyy8zwq9f8ml5rb3kcn6ajgbav9jg9ccvahf6m4p3dpz6";
        };
        #local_src = ./../../../../SDL_FontCache;
    in
      stdenv.mkDerivation rec {
        pname = "SDL_FontCache-unstable";
        version = "2021-03-29-ramirez7";

        src = gh_src;

        nativeBuildInputs = [ cmake ];
        buildInputs = [ SDL2 SDL2_ttf SDL_gpu ];

        cmakeFlags = [
          "-Dsdl2_INCLUDE_DIR=${SDL2.dev}/include/SDL2"
          "-Dsdl2_ttf_INCLUDE_DIR=${SDL2_ttf}/include/SDL2"
          "-Dsdl2_gpu_INCLUDE_DIR=${SDL_gpu}/include/SDL2"
        ];

        preInstall = ''
          echo "HELLO WTF"
          # pkg-config
          mkdir -p $out/lib/pkgconfig
          cat >> $out/lib/pkgconfig/SDL_FontCache.pc << EOF
          prefix=@CMAKE_INSTALL_PREFIX@
          exec_prefix=''${prefix}
          libdir=''${exec_prefix}/lib
          includedir=''${prefix}/include

          Name: SDL_FontCache
          Description: SDL_FontCache
          Version: ${version}
          Requires.private: sdl2 SDL_gpu
          Libs: -L$out/lib -lSDL_FontCache
          Libs.private: @private_libs@
          Cflags: -I$out/include/SDL2

          EOF
        '';

        meta = with lib; {
          description = "A generic font caching C library with loading and rendering support for SDL.";
          homepage = "https://github.com/grimfang4/SDL_FontCache";
          license = licenses.mit;
          maintainers = with maintainers; [ ramirez7 ];
          platforms = platforms.linux;
        };
      };
in
{
  SDL_FontCache = super.callPackage sdlFC {};
}
