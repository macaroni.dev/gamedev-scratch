self: super:
{
  # this is actually fixed in nixpkgs HEAD atm
  libdrm = super.libdrm.overrideAttrs(old: if !self.stdenv.hostPlatform.isMusl then {} else {
    mesonFlags = self.lib.lists.remove "-Dintel=false" old.mesonFlags;
  });
}
