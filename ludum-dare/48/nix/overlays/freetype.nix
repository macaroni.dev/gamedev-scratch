self: super:
{
  libpng = super.libpng.overrideAttrs(old: if !self.stdenv.hostPlatform.isWindows then {} else {
    # freetype on windows complains about the presence of the .la file
    # removing it seems to work
    postInstall = ''
      echo "HELLO LIBPNG FIXUP FOR WINDOWS"
      rm $out/lib/libpng16.la
    '';
  });
  freetype = super.freetype.overrideAttrs(old: if !self.stdenv.hostPlatform.isWindows then {} else {
    #buildInputs = old.buildInputs ++ [ self.libpng.dev self.libpng ];
  });
}
