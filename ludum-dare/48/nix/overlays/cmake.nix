self: super:
{
  cmake = (super.cmake.override (if !self.stdenv.hostPlatform.isWindows then {} else {
    useSharedLibraries = false; # will not taking into account isBoostrap be an issue I wonder?
  })).overrideAttrs (old: if !self.stdenv.hostPlatform.isWindows then {} else {
    buildInputs = old.buildInputs ++ [ self.openssl ];
    configureFlags = old.configureFlags ++ [ "-DCMAKE_FIND_DEBUG_MODE=ON" "-DCMAKE_SYSTEM_NAME=Windows"];
  });
}
