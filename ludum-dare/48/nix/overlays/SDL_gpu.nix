self: super:
# TODOs:
# - Build tests/demos on windows
# - Conditional tests/demos
{
  SDL_gpu = super.SDL_gpu.overrideAttrs (old: {
    pname = "SDL_gpu";
    #buildInputs = old.buildInputs ++ [ self.pkgconfig self.cmake self.doxygen self.graphviz ];
    #buildInputs = old.buildInputs ++ [ self.pkgconfig self.cmake self.doxygen self.graphviz ];
    #buildInputs = if !self.stdenv.hostPlatform.isWindows then old.buildInputs else [ self.cmake self.pkgconfig self.SDL2 ];

    # We don't need libGLU on Windows - mingw provides the OpenGL dlls we need
    buildInputs = if !self.stdenv.hostPlatform.isWindows then old.buildInputs else [ self.SDL2 ];

    # We need pkgconfig
    nativeBuildInputs = old.nativeBuildInputs ++ [ self.pkgconfig ];

    # NOTE: These flags change in HEAD
    cmakeFlags = [
      "-DSDL_gpu_BUILD_DEMOS=OFF"
      "-DSDL_gpu_BUILD_TOOLS=OFF"
      "-DSDL_gpu_BUILD_VIDEO_TEST=OFF"
      "-DSDL_gpu_BUILD_TESTS=OFF"
      "-DSDL_gpu_BUILD_DOCS=OFF"
      "-DSDL_gpu_INSTALL=ON" # Windows sets this to OFF
    ];

    preInstall = ''
      mkdir -p $out

      # demos
      #cp -r demos $out
      #cp -r $src/demos/data $out/demos

      #tests
      #cp -r tests $out

      # docs
      #make doc
      #mkdir -p $out/docs
      #cp -r html $out/docs

      # pkg-config
      mkdir -p $out/lib/pkgconfig
      cat >> $out/lib/pkgconfig/SDL_gpu.pc << EOF
      prefix=@CMAKE_INSTALL_PREFIX@
      exec_prefix=''${prefix}
      libdir=''${exec_prefix}/lib
      includedir=''${prefix}/include

      Name: SDL_gpu
      Description: SDL_gpu
      Version: ${old.version}
      Requires.private: sdl2
      Libs: -L$out/lib -lSDL2_gpu
      Libs.private: @private_libs@
      Cflags: -I$out/include/SDL2

      EOF
    '';
  });
}
