{
  patch = self: super:
    {
      rhash = super.rhash.overrideAttrs (old: if !self.stdenv.hostPlatform.isWindows then {} else rec {
        # https://github.com/NixOS/nixpkgs/issues/113742#issuecomment-782859473
        patches = [ ./rhash/set_host_os.patch ];

        configurePhase = ''
              export HOST_OS=MINGW64
              ./configure --prefix=$out
        '';
        postInstall = ''
          rm $out/lib/librhash.so
          cp $out/lib/librhash.dll.a $out/lib/LibRHash.dll.a
        '';
      });
    };
}
