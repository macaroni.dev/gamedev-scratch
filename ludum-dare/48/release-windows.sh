#! /usr/bin/env bash

set -ex

git diff --quiet ':(exclude)release-windows.sh'

t=$(date --rfc-3339=date | sed 's/ /-/g' | sed 's/:/-/g')
g=$(git rev-parse --short HEAD)
outdir="jam-windows-$t-$g"
releasework="releases/jam-windows-$t-$g"
releasedir="$releasework/NotScaredOfMyBasement"

./splice-for-windows.sh

nix-build -A projectCross.mingwW64.hsPkgs.ld48.components.exes.jam -o "$outdir"

mkdir -p "$releasedir"/assets/jam

cp "$outdir"/bin/* "$releasedir"

rm "$outdir"

cp -r assets/jam/* "$releasedir/assets/jam"

cp *.dll "$releasedir"

chmod +w -R "$releasedir"

pushd "$releasework"
zip -r NotScaredOfMyBasement.zip NotScaredOfMyBasement
popd

echo "Built $releasedir"
