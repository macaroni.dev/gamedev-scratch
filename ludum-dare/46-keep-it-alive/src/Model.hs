{-# LANGUAGE StrictData #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE DuplicateRecordFields #-}

module Model where

import Control.Lens ((.~), (%~))
import Data.Generics.Labels ()
import Data.Vector (Vector)
import qualified Data.Vector as Vector
import GHC.Generics (Generic)
import Data.Set (Set)
import qualified Data.Set as Set

import Dialog
import Objects
import Transmutation
import Types

data World = World
  { room :: RoomKey
  , roomMap :: F RoomKey Room
  , apprentice :: Apprentice
  , transmutation :: SomeTransmutation
  , gfxDebug :: Bool
  , dialog :: Maybe (Dialog World)
  } deriving (Show, Generic)

currRoom :: World -> Room
currRoom World{..} = unF roomMap room

withTransmutation :: Transmutation tier -> World -> World
withTransmutation t w = w { transmutation = SomeTransmutation $ Just t }

data RoomKey =
    Main
  | Library
  | Graveyard
  | Garden
  deriving (Eq, Show)

data Apprentice = Apprentice
  { location :: (Float, Float)
  , direction :: (Int, Int)
  , facing :: Dir
  , inventory :: Inventory
  , interaction :: Maybe Interaction
  } deriving (Show, Generic)

data Dir = DirLeft | DirRight | DirUp | DirDown deriving Show

resetFacing :: Apprentice -> Apprentice
resetFacing Apprentice{..} = Apprentice{facing=maybe facing id $ vecToDir direction, ..}

vecToDir :: (Int, Int) -> Maybe Dir
vecToDir (x, y) =
  if | x > 0 -> Just DirRight
     | x < 0 -> Just DirLeft
     | y > 0 -> Just DirDown
     | y < 0 -> Just DirUp
     | otherwise -> Nothing

data Inventory = Inventory
  { ingredients :: Set Ingredient
  , usedIngredients :: Set Ingredient
  , fetalRemains :: Bool
  , strangeTechnology :: Bool
  , soulStone :: Bool
  } deriving (Show, Generic)


emptyInventory :: Inventory
emptyInventory = Inventory
  { ingredients = mempty
  , usedIngredients = mempty
  , fetalRemains = False
  , strangeTechnology = False
  , soulStone = False
  }

-- TODO: Use Ix for all this?
data Grid a = Grid
  { tiles :: Vector (Vector a)
  } deriving (Eq, Show)

gridGet :: (Int, Int) -> Grid a -> Maybe a
gridGet (x, y) Grid{..} = do
  row <- tiles Vector.!? y
  row Vector.!? x

-- TODO: Abstract over Ix
ifoldMapGrid :: forall a b n. Num n => Monoid b => (n -> n -> a -> b) -> Grid a -> b
ifoldMapGrid f Grid{..} = Vector.ifoldl'
  (\acc1 y as -> Vector.ifoldl' (\acc2 x a -> f (fromIntegral x) (fromIntegral y) a <> acc2) acc1 as
  ) (mempty :: b) tiles

ifoldForGrid :: forall a b n. Num n => Monoid b => Grid a -> (n -> n -> a -> b) -> b
ifoldForGrid = flip ifoldMapGrid

-- | 1 px = 1 "unit"
--   1 tile = 32x32
data Tile = Floor RoomKey | Wall RoomKey | OffMap
  deriving (Eq, Show)

type TileGrid = Grid Tile

data Room = Room
  { grid :: TileGrid
  , objects :: [Object World]
  }
  deriving (Show, Generic)
