{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DuplicateRecordFields #-}
module Defs where

import CuteC2
import qualified Graphics.Gloss as Gloss
import Data.Generics.Labels ()
import Control.Lens ((&), (^.), (.~), (%~), _1, _2)
import Data.Set ((\\))
import qualified Data.Set as Set
import qualified Data.Map as Map
import qualified Data.Vector as Vector
import qualified Data.Foldable
import Data.List.NonEmpty (NonEmpty(..))
import qualified Data.Sequence as Seq
import qualified Data.List

import Model
import Dialog
import Types
import Assets
import Transmutation
import Objects

mainRoom :: Room
mainRoom =
  Room
  { grid =
      Grid $ Vector.fromList $
      Vector.replicate 16 OffMap : Vector.replicate 16 (Wall Library) :
      (Data.List.replicate 9 (Vector.replicate 16 (Floor Library))
      )
      ++ [Vector.replicate 16 (Floor Library)]
  , objects =
    [ mkObject transmuter (200, 200) (mkAABB 200 200)
      & #hitbox . #blocksMovement .~ True
      & addTrigger Hand (\w -> spawnDialog (transmutationDialog w) w)
    , mkObject door (800, 400) (mkAABB 64 64)
      & #hitbox . #blocksMovement .~ True
      & addDoorTo (1000, 300) Library
    ]

  }

libraryRoom :: Room
libraryRoom =
  Room
  { grid =
      Grid $ Vector.fromList $
      Vector.replicate 16 (Wall Library) : Vector.replicate 16 (Wall Library) :
      (Data.List.replicate 9 (Vector.replicate 16 (Floor Library))
      )
      ++ [Vector.replicate 16 (Floor Library)]
  , objects =
    [ mkBookshelf (0,   0)
    , mkBookshelf (140, 0)
    , mkBookshelf (280, 0)
    , mkBookshelf (420, 0)
    , mkBookshelf (560, 0)
    , mkBookshelf (700, 0)
    , mkBookshelf (840, 0)
    , mkTable     (560, 440)
    , mkTable     (100, 550)
    , mkTable     (100, 250)
    , mkObject otherWizard (50, 450) (mkAABB 64 180)
      & #hitbox . #blocksMovement .~ True
    ]
  }

testObj :: Object World
testObj =
  mkObject apprenticeUp (200, 200) (mkAABB 200 200)
--  & #objLoc ._1 .~ 1.0 -- TODO (WTF): Uncommenting these breaks compilation
--  & #objLoc ._2 .~ 1.0 -- Due to Generics errors
  & #hitbox . #blocksMovement .~ True
--  & #objLoc ._1 .~ 1.0 -- But this works .. wtf
--  & #objLoc ._2 .~ 1.0
  & addTrigger Hand (spawnDialogText "Enchanter goes here")
  & addTrigger Net (addToInventory (MindI Grimoire))

spawnDialogText :: String -> World -> World
spawnDialogText s w = w & #dialog .~ Just (dialogText Nothing Nothing s)

spawnDialog :: Dialog World -> World -> World
spawnDialog d w = w & #dialog .~ Just d

addToInventory :: Ingredient -> World -> World
addToInventory i w =
  if | Set.member i $ w ^. #apprentice . #inventory . #ingredients <> w ^. #apprentice . #inventory . #usedIngredients -> w
     | otherwise ->
       w
       & #apprentice . #inventory . #ingredients %~ Set.insert i
       & spawnDialogText ("Found " ++ humanIngredient i ++ "!")

addDoorTo :: (Float, Float) -> RoomKey -> Object World -> Object World
addDoorTo loc rk = addTrigger Hand (\w -> w & #room .~ rk & #apprentice . #location .~ loc)

mkIngredientSelect :: Ingredient -> DialogSelect Ingredient
mkIngredientSelect i = 
  DialogSelect
         { value = i
         , chosen = False
         , text = humanIngredient i
         }

transmutationDialog :: World -> Dialog World
transmutationDialog world =
  let ingrs = Data.Foldable.toList (world ^. #apprentice . #inventory . #ingredients)
  in if length ingrs < 3
  then dialogText Nothing Nothing "Need 3 ingredients to transmute"
  else DialogSelectThree
       { upOptions = mempty
       , currOption = mkIngredientSelect (head ingrs)
       , downOptions = Seq.fromList $ fmap mkIngredientSelect (tail ingrs)
       , done = False
       , next = F $ \(i1, i2, i3) -> case world ^. #transmutation of
           SomeTransmutation Nothing -> transmuteAndContinue TState0 i1 i2 i3
           SomeTransmutation (Just TDeath{}) -> transmuteAndContinue TState0 i1 i2 i3
           SomeTransmutation (Just TLife{state}) -> transmuteAndContinue state i1 i2 i3
       }

transmuteAndContinue :: TransmutationState tier -> Ingredient -> Ingredient -> Ingredient -> DialogStep World
transmuteAndContinue state i1 i2 i3 =
  case transmute (ingredientType i1) (ingredientType i2) (ingredientType i3) state of
    t@TDeath{death=TDMythic Body} ->
      DialogContinue (F $ resetIngredients . withTransmutation t . withFetalRemains) $
      DialogSequence $ dialogText Nothing Nothing "It's ripe..but it isn't ALIVE" :|
      [ dialogText (Just Gloss.red) Nothing "Got Fetal Remains"
      , dialogText Nothing Nothing "It died"
      , DialogEffect $ DialogDone $ F $ withTransmutation (TDeath $ TDGeneric "Dead Blood Bloom")
      ]
    t@TDeath{death=TDMythic Mind} ->
      DialogContinue (F $ resetIngredients . withTransmutation t . withStrangeTechnology) $
      DialogSequence $ dialogText Nothing Nothing "It glows with knowledge..but it isn't ALIVE" :|
      [ dialogText (Just Gloss.green) Nothing "Got Strange Technology"
      , dialogText Nothing Nothing "It died"
      , DialogEffect $ DialogDone $ F $ withTransmutation (TDeath $ TDGeneric "Dead Silicon Obelisk")
      ]
    t@TDeath{death=TDMythic Soul} ->
      DialogContinue (F $ resetIngredients . withTransmutation t . withSoulStone) $
      DialogSequence $ dialogText Nothing Nothing "A powerful soul..but it hasn't been ALIVE for a long time" :|
      [ dialogText (Just Gloss.blue) Nothing "Got Soul Stone"
      , dialogText Nothing Nothing "It died"
      , DialogEffect $ DialogDone $ F $ withTransmutation (TDeath $ TDGeneric "Dead Ancestral Spirit")
      ]
    t@TDeath{} -> DialogContinue (F $ resetIngredients . withTransmutation t) $ dialogText Nothing Nothing "It died"
    t@TLife{} ->
      DialogContinue (F $ useIngredients i1 i2 i3 . withTransmutation t) $
      DialogSequence $ dialogText Nothing Nothing "It isn't dead.." :|
      [ dialogText (Just Gloss.red) Nothing "KEEP IT ALIVE" ]

useIngredients :: Ingredient -> Ingredient -> Ingredient -> World -> World
useIngredients i1 i2 i3 w =
  w
  & #apprentice . #inventory . #ingredients %~ (\\ Set.fromList [i1, i2, i3])
  & #apprentice . #inventory . #usedIngredients %~ (<> Set.fromList [i1, i2, i3])

resetIngredients :: World -> World
resetIngredients w = w & #apprentice . #inventory . #usedIngredients .~ mempty

withFetalRemains :: World -> World
withFetalRemains w = w & #apprentice . #inventory . #fetalRemains .~ True

withStrangeTechnology :: World -> World
withStrangeTechnology w = w & #apprentice . #inventory . #strangeTechnology .~ True

withSoulStone :: World -> World
withSoulStone w = w & #apprentice . #inventory . #soulStone .~ True

mkBookshelf :: (Float, Float) -> Object World
mkBookshelf loc =
  mkObject bookshelf loc (mkAABB 128 128)
  & #hitbox . #blocksMovement .~ True

mkTable :: (Float, Float) -> Object World
mkTable loc =
  mkObject table loc (mkAABB 194 64)
  & #hitbox . #blocksMovement .~ True
