{-# LANGUAGE StrictData #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE DuplicateRecordFields #-}

module Game where

import CuteC2
import qualified Data.List
import Data.List.NonEmpty(NonEmpty(..))
import Control.Lens ((^.), (.~), (&), (%~), _1, _2)
import qualified Control.Lens as Lens
import Data.Generics.Labels ()
import qualified Graphics.Gloss as Gloss
import qualified Graphics.Gloss.Juicy as Gloss
import qualified Graphics.Gloss.Interface.IO.Game as Gloss
import Data.Vector (Vector)
import qualified Data.Vector as Vector
import Debug.Trace (trace)
import qualified System.IO
import qualified Data.Set as Set
import Debug.Trace
import qualified Data.Map.Strict as Map

import Dialog
import Model
import Types
import Objects
import Transmutation
import Assets
import qualified Defs

renderWorld :: Assets -> World -> Gloss.Picture
renderWorld assets@Assets{..} w@World{..} =
  let Room{..} = currRoom w
      (apprenticeX, apprenticeY) = apprentice ^. #location
      apprenticeDir = apprentice ^. #facing
   in
    (
      -- Render grid
      (
        ifoldForGrid grid $ \x y tile -> fixTranslate (x*64) (y*64) (mwhen gfxDebug (debugCoord x y) <> tileAsset assets tile)
      ) <>
      -- Render objects
      (
        foldFor objects $ \obj ->
          let ObjectPicture{location=(x,y),..} = renderObject assets obj
           in fixTranslate x y $ mconcat
              [ picture
              , mwhen gfxDebug hitboxPicture
              ]
      ) <>
      -- Render transmutation
      (
        mwhen (room == Main) $ (fixTranslate 200 200 $ renderTransmutation assets transmutation)
      ) <>
      -- Render apprentice
      (
        fixTranslate apprenticeX apprenticeY ( -- NOTE: y - 64??
          apprenticeAsset assets apprentice <>
            mwhen gfxDebug (
              Gloss.Color Gloss.red (Gloss.circle 32) <>
              renderAABB apprenticeAABB
              )
            
          ) <>
        (
          uncurry fixTranslate (interactionLoc apprentice) (renderAABB (interactionAABB apprentice))
        )
      ) <>
      -- Render dialog
      (
        maybe mempty id $ do
          DialogPicture{location=(x,y), picture} <- dialog >>= renderDialog
          pure $ fixTranslate x y picture
      ) <>
      -- Misc gfxDebug
      (
        mwhen gfxDebug (
          (ifoldForGrid grid (\x y _ -> debugCoord @Int x y)) <>
          (Gloss.Color Gloss.red $ Gloss.translate 0 (-256) $ Gloss.circle 32)
          )
      )
    )

fixTranslate :: Float -> Float -> Gloss.Picture -> Gloss.Picture
fixTranslate x y =
  Gloss.translate
  (x + ((64 - screenWidthF) / 2))
  (((screenHeightF - 64) / 2) - y)

debugCoord :: Show a => a -> a -> Gloss.Picture
debugCoord x y = Gloss.scale 0.1 0.1 $ Gloss.color Gloss.white $ Gloss.text $ show (x,y)

main :: IO ()
main = do
  System.IO.hSetBuffering System.IO.stdout System.IO.NoBuffering
  assets <- loadAssets
      -- TODO: Use a functional approach to room building
  let world = World
        { room = Library
        , roomMap = F $ \case
            Main -> Defs.mainRoom
            Library -> Defs.libraryRoom
            _ -> Defs.mainRoom
        , apprentice = Apprentice
          { location = (500, 500)
          , direction = (0, 0)
          , facing = DirDown
          , inventory = emptyInventory
            { ingredients = allIngredients
            , fetalRemains = True
            }
                                       
          , interaction = Nothing
          }
        , transmutation = SomeTransmutation Nothing
        , gfxDebug = False
        , dialog = Nothing
        }
      rendered = renderWorld assets world
      something = mungeCenter $ mconcat
        [ Gloss.translate 100 100 $ Gloss.Color Gloss.yellow $ Gloss.Circle 32
        , Gloss.translate 0 100 $ Gloss.Color Gloss.red $ Gloss.Circle 32
        , Gloss.translate 100 0 $ Gloss.Color Gloss.green $ Gloss.Circle 32
        ]

      debugEvent e w = do
        putStr $ "\r" ++ show e
        pure w
      inWindow = (Gloss.InWindow "A Mage's Thesis" (screenWidth, screenHeight) (0, 0))
  Gloss.playIO inWindow  Gloss.black 60 world (\w -> pure $ renderWorld assets w) (\e w -> pure $ handleWorld e w) tickWorldIO
--  Gloss.play (Gloss.InWindow "hello" (1024, 768) (10, 10)) Gloss.black 60

screenHeight :: Int
screenWidth :: Int
screenHeight = 768
screenWidth = 1024


screenHeightF :: Float
screenWidthF :: Float
screenHeightF = fromIntegral screenHeight
screenWidthF = fromIntegral screenWidth
foldFor :: Foldable f => Monoid b => f a -> (a -> b) -> b
foldFor = flip foldMap

-- TODO: Abstract over tilesize
mungeCenter :: Gloss.Picture -> Gloss.Picture
mungeCenter = Gloss.translate (fromIntegral (64 - screenWidth) / 2) (fromIntegral (screenHeight - 64) / 2)

---

handleWorld :: Gloss.Event -> World -> World
handleWorld e = \case
  world@World{dialog=Nothing} -> handleMenu e $ handleDebug e $ handleApprenticeInteraction e $ handleApprenticeMovement e world
  world@World{dialog=Just dialog} -> handleApprenticeMovement e $ case handleDialog e dialog of
    d' -> world & #dialog .~ (Just d')

handleMenu :: Gloss.Event -> World -> World
handleMenu e w@World{..} =
  let Inventory{..} = apprentice ^. #inventory
  in case e of
  Gloss.EventKey (Gloss.SpecialKey Gloss.KeyEnter) Gloss.Down _ _ ->
    World { dialog = Just $ DialogListView
            { items = mconcat
                      [ [DialogListItem "Tools: Net [ J ] Shovel [ K ] Scissors [ L ]" Gloss.white]
                      , [DialogListItem "--------------------------------" Gloss.white]
                      , [DialogListItem "Inventory:" Gloss.white]
                      , [DialogListItem "" Gloss.white]
                      , mwhen fetalRemains [DialogListItem "FETAL REMAINS" Gloss.red]
                      , mwhen strangeTechnology [DialogListItem "STRANGE TECHNOLOGY" Gloss.green]
                      , mwhen soulStone [DialogListItem "SOUL STONE" Gloss.blue]
                      , flip fmap (Set.toList ingredients) $ \i -> DialogListItem (humanIngredient i) Gloss.white
                      ]
            , done = False
            , pageSize = 20
            , pageStart = 0
            }, ..
          }
  _ -> w

handleDebug :: Gloss.Event -> World -> World
handleDebug e w@World{..} = case e of
  Gloss.EventKey (Gloss.Char 'm') Gloss.Down _ _ ->
    traceShowId w
  Gloss.EventKey (Gloss.Char 'g') Gloss.Down _ _ ->
    World{gfxDebug = not gfxDebug, ..}
  Gloss.EventKey (Gloss.Char 'o') Gloss.Down _ _ ->
    World{ dialog = Just $ DialogSequence $
             dialogText Nothing Nothing "hello this is a mage's thesis" :|
           [ dialogText Nothing Nothing "Please enjoy the game, and remember..."
           , DialogEffect $ DialogDone $ F $ \world -> world & #apprentice . #location .~ (500, 500)
           , dialogText (Just Gloss.red) Nothing "KEEP    IT     ALIVE"
           ]
         , .. }
  _ -> w

handleApprenticeInteraction :: Gloss.Event -> World -> World
handleApprenticeInteraction e world = case e of
  Gloss.EventKey k ks _ _ -> case (k, ks) of
    (Gloss.SpecialKey Gloss.KeySpace, Gloss.Down) ->
      world
      & #apprentice . #interaction .~ Just Hand
    (Gloss.Char 'j', Gloss.Down) ->
      world
      & #apprentice . #interaction .~ Just Net
    (Gloss.Char 'k', Gloss.Down) ->
      world
      & #apprentice . #interaction .~ Just Shovel
    (Gloss.Char 'l', Gloss.Down) ->
      world
      & #apprentice . #interaction .~ Just Scissors
    _ -> world
  _ -> world


handleApprenticeMovement :: Gloss.Event -> World -> World
handleApprenticeMovement e world = case e of
  Gloss.EventKey k ks _ _ -> case (k, ks) of
    (Gloss.Char 'a', Gloss.Down) ->
      world
      & #apprentice . #direction . _1 %~ pred
      & #apprentice . #facing .~ DirLeft
    (Gloss.Char 'a', Gloss.Up) ->
      world
      & #apprentice . #direction . _1 %~ succ
      & #apprentice %~ resetFacing

    (Gloss.Char 'd', Gloss.Down) ->
      world
      & #apprentice . #direction . _1 %~ succ
      & #apprentice . #facing .~ DirRight
    (Gloss.Char 'd', Gloss.Up) ->
      world
      & #apprentice . #direction . _1 %~ pred
      & #apprentice %~ resetFacing

    (Gloss.Char 'w', Gloss.Down) ->
      world
      & #apprentice . #direction . _2 %~ pred
      & #apprentice . #facing .~ DirUp
    (Gloss.Char 'w', Gloss.Up) ->
      world
      & #apprentice . #direction . _2 %~ succ
      & #apprentice %~ resetFacing

    (Gloss.Char 's', Gloss.Down) ->
      world
      & #apprentice . #direction . _2 %~ succ
      & #apprentice . #facing .~ DirDown
    (Gloss.Char 's', Gloss.Up) ->
      world
      & #apprentice . #direction . _2 %~ pred
      & #apprentice %~ resetFacing

    _ -> world
  _ -> world

-- Probably use stateT
tickWorldIO :: a -> World -> IO World
tickWorldIO _ w = do
  let w' = tickWorld w
--  putStrLn $ show w'
--  putStrLn "-------------------------"
  pure w'

tickWorld :: World -> World
tickWorld = \case
  world@World{dialog=Nothing} -> tickApprentice world
  world@World{dialog=Just dialog} -> case tickDialog dialog of
    (d', f) -> f world & #dialog .~ d'

tickApprentice :: World -> World
tickApprentice world =
  let (dirx, diry) = world ^. #apprentice . #direction
      (dx, dy) = (fromIntegral dirx * apprenticeVelocity, fromIntegral diry * apprenticeVelocity)
   in
       world
       & #apprentice . #location %~ moveAndClamp (dx, dy)
       & tickInteraction
       & #apprentice . #interaction .~ Nothing
  where
    tickInteraction :: World -> World
    tickInteraction w@World{..} = case apprentice of
      Apprentice{interaction=Nothing} -> w
      Apprentice{interaction=Just interaction, ..} ->
        nothingHappened interaction $
        objectInteract interaction (interactionLoc apprentice) (interactionAABB apprentice) (currRoom world ^. #objects) w
    moveAndClamp :: (Float, Float) -> (Float, Float) -> (Float, Float)
    moveAndClamp (dx, dy) (currX, currY) =
      let nextLoc@(nextX, nextY) = (currX + dx, currY + dy)
          nextTileLoc = (floor $ nextX / 64, floor $ nextY / 64)
          grid = currRoom world ^. #grid
       in case gridGet nextTileLoc grid of
        Just (Floor _) ->
          if movementBlocked nextLoc apprenticeAABB (currRoom world ^. #objects)
           then (currX, currY)
           else nextLoc
        _ -> (currX, currY)

putWords :: [String] -> IO ()
putWords = putStr . unwords

putWordsLn :: [String] -> IO ()
putWordsLn = putStrLn . unwords

apprenticeVelocity :: Float
apprenticeVelocity = 5.0

-- The engine currently projects AABB from origin (top-left)
-- which has the unfortunate side-effect of making it so the
-- apprentice's head must have a hitbox -_-
apprenticeAABB :: C2AABB
apprenticeAABB = C2AABB
  { c2AABB_min = C2V 0 0
  , c2AABB_max = C2V 48 96
  }

interactionAABB :: Apprentice -> C2AABB
interactionAABB Apprentice{facing} = case facing of
  DirLeft ->
    C2AABB
    { c2AABB_min = C2V 0 0
    , c2AABB_max = C2V (-2 * interactionSize) interactionSize
    }
  DirRight ->
    C2AABB
    { c2AABB_min = C2V 0 0
    , c2AABB_max = C2V (2 * interactionSize) interactionSize
    }
  DirUp ->
    C2AABB
    { c2AABB_min = C2V 0 0
    , c2AABB_max = C2V interactionSize (-2 * interactionSize)
    }
  DirDown ->
    C2AABB
    { c2AABB_min = C2V 0 0
    , c2AABB_max = C2V interactionSize (2 * interactionSize)
    }
interactionSize :: Float
interactionSize = 32

interactionLoc :: Apprentice -> (Float, Float)
interactionLoc Apprentice{location=(x, y), facing} = case facing of
  DirLeft ->  (x-(2*interactionSize), y)
  DirRight -> (x+interactionSize, y)
  DirUp -> (x, y-(2*interactionSize))
  DirDown -> (x, y+interactionSize)

mwhen :: Monoid a => Bool -> a -> a
mwhen True a = a
mwhen False _ = mempty

munless :: Monoid a => Bool -> a -> a
munless False a = a
munless True _ = mempty

apprenticeAsset :: Assets -> Apprentice -> Gloss.Picture
apprenticeAsset Assets{..} Apprentice{facing} = case facing of
  DirDown -> apprenticeDown
  DirUp -> apprenticeUp
  DirLeft -> apprenticeLeft
  DirRight -> apprenticeRight

tileAsset :: Assets -> Tile -> Gloss.Picture
tileAsset Assets{..} = \case
  Floor Library -> libraryFloor
  Wall Library -> libraryWall
  OffMap -> offmap
  _ -> error "WIRE THAT ASSET UP NOW"

nothingHappened :: Interaction -> World -> World
nothingHappened i = \case
  w@World{dialog=Just{}} -> w
  w@World{dialog=Nothing} -> w & #dialog .~ Just (dialogText Nothing Nothing $ unwords ["("++show i++")", "Nothing happened..."])

renderTransmutation :: Assets -> SomeTransmutation -> Gloss.Picture
renderTransmutation Assets{..} = \case
  SomeTransmutation Nothing -> mempty
  SomeTransmutation (Just TDeath{death}) -> case death of
    TDMythic Body -> td3Body
    TDMythic Mind -> td3Mind
    TDMythic Soul -> td3Soul
    TDDominant1 Body -> deadBody
    TDDominant1 Mind -> deadMind
    TDDominant1 Soul -> deadSoul
    _ -> deadSplat
  SomeTransmutation (Just TLife{life}) -> case life of
    TLBalanced1 -> tl1Balanced
    TLDominant1 Mind -> tl1Mind
    TLDominant1 Body -> tl1Body
    TLDominant1 Soul -> tl1Soul
    TLBalanced2 -> tl2Balanced
    TLDominant2 Mind -> tl2Mind
    TLDominant2 Body -> tl2Body
    TLDominant2 Soul -> tl2Soul
    TLDual2 Mind Mind -> tl2Mind
    TLDual2 Mind Body -> tl2MindBody
    TLDual2 Mind Soul -> tl2MindSoul
    
    TLDual2 Body Body -> tl2Body
    TLDual2 Body Mind -> tl2MindBody
    TLDual2 Body Soul -> tl2SoulBody
    
    TLDual2 Soul Soul -> tl2Mind
    TLDual2 Soul Body -> tl2SoulBody
    TLDual2 Soul Mind -> tl2MindSoul
