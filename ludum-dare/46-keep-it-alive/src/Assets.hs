{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE LambdaCase #-}

module Assets where

import qualified Graphics.Gloss as Gloss
import qualified Graphics.Gloss.Juicy as Gloss
import GHC.Generics (Generic)

-- Assets are consolidated here so we can easily
-- scaleAssets :: Float -> Assets -> Assets for screen sizes
-- or different rooms..maybe scaleAssets just goes in World?
data Assets = Assets
  { apprenticeLeft :: Gloss.Picture
  , apprenticeRight :: Gloss.Picture
  , apprenticeUp :: Gloss.Picture
  , apprenticeDown :: Gloss.Picture
  , libraryFloor :: Gloss.Picture
  , libraryWall :: Gloss.Picture
  , offmap :: Gloss.Picture
  , door :: Gloss.Picture
  , transmuter :: Gloss.Picture
  , deadMind :: Gloss.Picture
  , deadBody :: Gloss.Picture
  , deadSoul :: Gloss.Picture
  , deadSplat :: Gloss.Picture
  , tl1Balanced :: Gloss.Picture
  , tl1Body :: Gloss.Picture
  , tl1Mind :: Gloss.Picture
  , tl1Soul :: Gloss.Picture
  , tl2Balanced :: Gloss.Picture
  , tl2Body :: Gloss.Picture
  , tl2Mind :: Gloss.Picture
  , tl2Soul :: Gloss.Picture
  , tl2MindBody :: Gloss.Picture
  , tl2MindSoul :: Gloss.Picture
  , tl2SoulBody :: Gloss.Picture
  , td3Body :: Gloss.Picture
  , td3Mind :: Gloss.Picture
  , td3Soul :: Gloss.Picture
  , bookshelf :: Gloss.Picture
  , table :: Gloss.Picture
  , otherWizard :: Gloss.Picture
  }

loadAssets :: IO Assets
loadAssets = do
  apprenticeLeft <- moveSprite 32 32 <$> loadPng "assets/ApprenticeLeft.png"
  apprenticeRight <- moveSprite 32 32 <$> loadPng "assets/ApprenticeRight.png"
  apprenticeUp <- moveSprite 32 32 <$> loadPng "assets/ApprenticeBack.png"
  apprenticeDown <- moveSprite 32 32 <$> loadPng "assets/ApprenticeFront.png"
  libraryFloor <- loadPng "assets/library-floor.png"
  libraryWall <- loadPng "assets/library-wall.png"
  bookshelf <- moveSprite 64 64 <$> loadPng "assets/Bookshelf.png"
  table <- moveSprite 92 32 <$> loadPng "assets/Table.png"
  otherWizard <- moveSprite 64 64 <$> loadPng "assets/OtherWizard.png"
  door <- moveTile <$> loadPng "assets/Doorway.png"
  
  -- transmutation
  transmuter   <- moveSprite 96 96 <$> loadPng "assets/Transmuter.png"
  deadMind     <- moveSprite 96 128 <$> loadPng "assets/DeadMind.png"
  deadBody     <- loadPng "assets/DeadBody.png"
  deadSoul     <- loadPng "assets/DeadSoul.png"
  deadSplat    <- loadPng "assets/DeadSplat.png"  
  tl1Balanced  <- loadPng "assets/Lvl1Balanced.png"
  tl1Body      <- loadPng "assets/Lvl1Body.png"
  tl1Mind      <- loadPng "assets/Lvl1Mind.png"
  tl1Soul      <- loadPng "assets/Lvl1Soul.png"
  tl2Balanced  <- loadPng "assets/Lvl2Balanced.png"
  tl2Body      <- loadPng "assets/Lvl2Body.png"
  tl2Mind      <- loadPng "assets/Lvl2Mind.png"
  tl2Soul      <- loadPng "assets/Lvl2Soul.png"
  tl2MindBody  <- loadPng "assets/Lvl2MindBody.png"
  tl2MindSoul  <- loadPng "assets/Lvl2MindSoul.png"
  tl2SoulBody  <- loadPng "assets/Lvl2SoulBody.png"
  td3Body      <- loadPng "assets/Lvl3Body.png"
  td3Mind      <- loadPng "assets/Lvl3Mind.png"
  td3Soul      <- loadPng "assets/Lvl3Soul.png"

  let offmap = mempty
  pure Assets{..}

moveTile :: Gloss.Picture -> Gloss.Picture
moveTile = Gloss.translate (32) (-32)

moveSprite :: Float -> Float -> Gloss.Picture -> Gloss.Picture
moveSprite x y = Gloss.translate (x) (-(y))

loadPng :: String -> IO Gloss.Picture
loadPng s = maybe (error $ "Unable to find file " ++ s) (Gloss.scale 2 2) <$> Gloss.loadJuicy s
