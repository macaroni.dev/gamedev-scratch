{-# LANGUAGE StrictData #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}

-- TODO POC:
-- - Put objects in the world (object is prob new module)
-- - Interact with objects
-- - Hit button to open dialog with count of interactions
-- THEN: DO a dialog/menu-only version of the game, integ'ing
-- transmutations. Ideally, scrolling menu :O could be re-used
-- for inventory later, and allows us to showcase the game NOW
-- Put MC in there too !
module Dialog where


import qualified Graphics.Gloss as Gloss
import qualified Graphics.Gloss.Interface.IO.Game as Gloss

import Data.Sequence (Seq)
import qualified Data.Sequence as Seq
import qualified Data.Foldable
import qualified Data.List
import Data.List.NonEmpty (NonEmpty(..), nonEmpty)
import qualified Data.List.NonEmpty as NonEmpty

import Types

data DialogStep world =
    DialogDone (F world world)
  | DialogContinue (F world world) (Dialog world)
  deriving Show

data DialogPicture = DialogPicture
  { location :: (Float, Float)
  , picture :: Gloss.Picture
  }


renderDialog :: Dialog world -> Maybe DialogPicture
renderDialog = \case
  DialogText{anchor=(x, y), ..} ->
    Just $
    DialogPicture (x + 512 + 64 + 2, y + 100) $ mconcat
    [ Gloss.Color Gloss.black $ Gloss.rectangleSolid 1024 100
    , Gloss.Color color $ Gloss.translate (-450) 0 $ Gloss.scale scale scale $ Gloss.Text text
    ]
  DialogListView{..} ->
    Just $ DialogPicture (0, 0) $ mconcat
    [ Gloss.Color Gloss.black $ Gloss.rectangleSolid 1024 1024
    , mconcat $ spacePictures 5 $ fmap (\DialogListItem{..} -> mkText color text) (paginate pageStart pageSize items)
    ]
  DialogSelectThree{..} ->
    Just $ DialogPicture (0, 0) $ mconcat
    [ Gloss.Color Gloss.black $ Gloss.rectangleSolid 1024 1024
    , mconcat $
      spacePictures 5 $
      Data.Foldable.toList $
      let top = seqLastN 5 $ fmap (mkText Gloss.white . renderSelect) upOptions
          bot = Seq.take (10 - length top) $ fmap (mkText Gloss.white . renderSelect) downOptions
      in
        pure (mkText Gloss.white "Select 3 ingredients to transmute") <>
        pure (mkText Gloss.white "Choose [ENTER] Transmute [SPACE] Exit [ESC]") <>
        pure (mkText Gloss.white "---------------------------------") <>
        top <>
        pure (mkText Gloss.white $ '>' : renderSelect currOption) <>
        bot
    ]
  DialogSequence (d :| _) -> renderDialog d
  DialogEffect{} -> Nothing

handleDialog :: Gloss.Event -> Dialog world -> Dialog world
handleDialog e dialog = case dialog of
  d@DialogText{} -> case e of
    Gloss.EventKey (Gloss.SpecialKey Gloss.KeyEnter) Gloss.Down _ _ -> d { done = True }
    _ -> d
  d@DialogListView{..} -> case e of
    Gloss.EventKey (Gloss.SpecialKey Gloss.KeyEnter) Gloss.Down _ _ ->
      if pageStart + pageSize >= length items
      then d { done = True }
      else d { pageStart = pageStart + pageSize }

    _ -> d

  d@DialogSelectThree{..} -> case e of
    Gloss.EventKey (Gloss.SpecialKey Gloss.KeyEsc) Gloss.Down _ _ ->
      DialogEffect (DialogDone (F id))
    Gloss.EventKey (Gloss.SpecialKey Gloss.KeyEnter) Gloss.Down _ _ ->
      let DialogSelect{..} = currOption
          selected = findSelected $ Data.Foldable.toList $ mconcat [upOptions, pure currOption, downOptions]
      in if not chosen && length selected >= 3 then d else DialogSelectThree{currOption=DialogSelect{chosen=not chosen, ..}, ..}
    Gloss.EventKey (Gloss.SpecialKey Gloss.KeySpace) Gloss.Down _ _ ->
      d { done = True }
    Gloss.EventKey (Gloss.SpecialKey Gloss.KeySpace) Gloss.Up _ _ ->
      d { done = False }
    Gloss.EventKey (Gloss.Char 'w') Gloss.Down _ _ | Seq.null upOptions -> d
    Gloss.EventKey (Gloss.Char 'w') Gloss.Down _ _ ->
      let upLast = Data.Foldable.length upOptions - 1
       in DialogSelectThree
          { upOptions = Seq.deleteAt upLast upOptions
          , currOption = Seq.index upOptions upLast
          , downOptions = currOption Seq.<| downOptions
          , ..
          }
    Gloss.EventKey (Gloss.Char 's') Gloss.Down _ _ | Seq.null downOptions -> d
    Gloss.EventKey (Gloss.Char 's') Gloss.Down _ _ ->
      DialogSelectThree
      { upOptions = upOptions Seq.|> currOption
      , currOption = Seq.index downOptions 0
      , downOptions = Seq.deleteAt 0 downOptions
      , ..
      }
    _ -> d
  DialogSequence (d :| ds) -> DialogSequence (handleDialog e d :| ds)
  DialogEffect{} -> dialog

dialogText :: Maybe Gloss.Color -> Maybe Float -> String -> Dialog world
dialogText mcolor mscale str = DialogText
  { anchor = (-100, 600)
  , text = str
  , color = maybe Gloss.white id mcolor
  , done = False
  , choices = Nothing
  , scale = maybe 0.2 id mscale
  }

-- TODO: Parameterize over a lens to automate the putting of the dialog back into world
tickDialog :: Dialog world -> (Maybe (Dialog world), world -> world)
tickDialog d = case stepDialog d of
  DialogDone (F f) -> (Nothing, f)
  -- Mind the prime! A linear type would help here :) 
  DialogContinue (F f) d' -> (Just d', f)

stepDialog :: Dialog world -> DialogStep world
stepDialog = \case
  d@DialogText{..} ->
    if done
    then maybe (DialogDone (F id)) (result . currChoice) choices
    else DialogContinue (F id) d
  d@DialogListView{..} ->
    if done
    then DialogDone (F id)
    else DialogContinue (F id) d
  d@DialogSelectThree{..} ->
    case findSelected $ Data.Foldable.toList $ mconcat [upOptions, pure currOption, downOptions] of
      [a1, a2, a3] | done -> unF next (a1, a2, a3)
      _ -> DialogContinue (F id) d
  DialogSequence (d :| ds) -> case stepDialog d of
    DialogDone f -> maybe (DialogDone f) (DialogContinue f . DialogSequence) (nonEmpty ds)
    DialogContinue f d' -> DialogContinue f (DialogSequence $ d' :| ds)
  DialogEffect step -> step
  
-- TODO: Add background to use this for full-on cutscenes!
data Dialog world =
    DialogText
    { anchor :: (Float, Float)
    , text :: String
    , color :: Gloss.Color
    , scale :: Float
    , done :: Bool
    , choices :: Maybe (DialogChoices world)
    }
  | DialogListView
    { items :: [DialogListItem]
    , pageStart :: Int
    , pageSize :: Int
    , done :: Bool
    }
  | forall a. Show a => DialogSelectThree
    { upOptions :: Seq (DialogSelect a)
    , currOption :: DialogSelect a
    , downOptions :: Seq (DialogSelect a)
    , done :: Bool
    , next :: F (a, a, a) (DialogStep world)
    }
  | DialogSequence (NonEmpty (Dialog world))
  | DialogEffect (DialogStep world)

deriving instance (Show (Dialog world))

data DialogSelect a = DialogSelect
  { value :: a
  , chosen :: Bool
  , text :: String
  } deriving Show

renderSelect :: DialogSelect a -> String
renderSelect DialogSelect{..} =
  if chosen then "*" ++ text else text

data DialogListItem = DialogListItem
  { text :: String
  , color :: Gloss.Color
  } deriving Show

-- TODO: Add more UIs to DialogChoices to have it handle menus & transmutation
data DialogChoices world = DialogChoices
    { upChoices :: Seq (DialogChoice world)
    , currChoice :: DialogChoice world
    , downChoices :: Seq (DialogChoice world)
    , selected :: Bool
    } deriving Show

data DialogChoice world = DialogChoice { name :: String, result :: DialogStep world } deriving Show

data TextChunk =
    LitChunk String
  | ColorChunk TextChunk
  deriving (Eq, Show)


-- Menu stuff
spacePictures :: Float -> [Gloss.Picture] -> [Gloss.Picture]
spacePictures space = fmap (\(dy, pic) -> Gloss.translate 0 (-dy * space) pic) . zip (iterate (+space) 0) 

mkText :: Gloss.Color -> String -> Gloss.Picture
mkText color = Gloss.color color . Gloss.scale 0.15 0.15 . Gloss.Text

paginate :: Int -> Int -> [a] -> [a]
paginate start size = take size . drop start

seqLastN :: Int -> Seq a -> Seq a
seqLastN n = Seq.reverse . Seq.take n . Seq.reverse

findSelected :: [DialogSelect a] -> [a]
findSelected = fmap (\DialogSelect{..} -> value) . filter (\DialogSelect{..} -> chosen)
