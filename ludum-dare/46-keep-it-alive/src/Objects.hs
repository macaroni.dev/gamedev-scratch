{-# LANGUAGE StrictData #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE DeriveGeneric #-}

module Objects where

import Control.Lens ((&), (^.), (.~), (%~))
import CuteC2
import qualified Graphics.Gloss as Gloss
import Data.Generics.Labels ()
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Monoid (Endo(..))
import Control.Monad (guard)
import GHC.Generics

import Types
import Assets

import Debug.Trace

data Interaction = Hand | Shovel | Scissors | Net deriving (Eq, Ord, Show)

data Hitbox world = Hitbox
  { hitShape :: C2AABB
  , blocksMovement :: Bool
  , triggers :: Map Interaction (F world world)
  } deriving (Show, Generic)

aabbAt :: (Float, Float) -> C2AABB -> C2AABB
aabbAt (x, y) C2AABB{c2AABB_min=C2V{c2v_x=minX, c2v_y=minY}, c2AABB_max=C2V{c2v_x=maxX, c2v_y=maxY}} =
  C2AABB
  { c2AABB_min = C2V{c2v_x=minX+x, c2v_y=minY+y}
  , c2AABB_max = C2V{c2v_x=maxX+x, c2v_y = maxY+y}
  }

data Object world = Object
  { hitbox :: Hitbox world
  , objLoc :: (Float, Float)
  , sprite :: F Assets Gloss.Picture
  } deriving (Show, Generic)

data ObjectPicture = ObjectPicture
  { location :: (Float, Float)
  , picture :: Gloss.Picture
  , hitboxPicture :: Gloss.Picture
  } deriving Show

emptyObject :: Object world
emptyObject = Object
  { hitbox = Hitbox
    { hitShape =C2AABB
      { c2AABB_min = C2V{c2v_x=0, c2v_y=0}
      , c2AABB_max = C2V{c2v_x=0, c2v_y = 0}
      }
    , blocksMovement = False
    , triggers = mempty
    }
  , objLoc = (0, 0)
  , sprite = F (const mempty)
  }

mkObject :: (Assets -> Gloss.Picture) -> (Float, Float) -> C2AABB -> Object world
mkObject sprite loc aabb =
  emptyObject
  & #sprite .~ F sprite
  & #hitbox . #hitShape .~ aabb
  & #objLoc .~ loc

addTrigger :: Interaction -> (world -> world) -> Object world -> Object world
addTrigger inter trigger obj = obj & #hitbox . #triggers %~ Map.insert inter (F trigger)

aabbDimensions :: C2AABB -> (Float, Float)
aabbDimensions C2AABB{c2AABB_min=C2V{c2v_x=minX, c2v_y=minY}, c2AABB_max=C2V{c2v_x=maxX, c2v_y=maxY}} =
  (abs (maxX - minX), abs (maxY - minY))

renderObject :: Assets -> Object world -> ObjectPicture
renderObject assets Object{hitbox=Hitbox{..},..} =
  ObjectPicture
  { location = objLoc
  , picture = unF sprite assets
  , hitboxPicture = renderAABB hitShape
  }

renderAABB :: C2AABB -> Gloss.Picture
renderAABB aabb =
  let (w, h) = aabbDimensions aabb
   in Gloss.translate (w/2) (-h/2) $
      Gloss.Color (Gloss.withAlpha 0.5 Gloss.red) $
      Gloss.rectangleSolid w h

mkAABB :: Float -> Float -> C2AABB
mkAABB x y = C2AABB (C2V 0 0) (C2V x y)

-- No broad phase. We'll just loop through all N objects in a room always
movementBlocked :: (Float, Float) -> C2AABB -> [Object world] -> Bool
movementBlocked loc shape =
  any $ \Object{hitbox=Hitbox{..}, objLoc=hitLoc} ->
    blocksMovement &&
    c2Collided (C2AABB' $ aabbAt loc shape) (C2AABB' $ aabbAt hitLoc hitShape)

objectInteract :: Interaction -> (Float, Float) -> C2AABB -> [Object world] -> world -> world
objectInteract interaction loc shape =
  appEndo . foldMap (
    \obj@Object{hitbox=Hitbox{..}, objLoc=hitLoc} -> Endo $ \world -> maybe world id $ do
      traceM $ unwords ["Loc =", show loc, "Shape =", show shape, "Object =", show obj]
      guard $ c2Collided (C2AABB' $ aabbAt loc shape) (C2AABB' $ aabbAt hitLoc hitShape)
      F trigger <- Map.lookup interaction triggers
      pure $ trigger world
    )
