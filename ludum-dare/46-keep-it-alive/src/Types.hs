module Types where

newtype F a b = F { unF :: (a -> b) }

instance Show (F a b) where
  show _ = "<func>"

